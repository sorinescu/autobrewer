#include "statemodel.h"
#include <QQmlEngine>
#include <QDataStream>

StateModel::StateModel(QObject *parent)
    : QAbstractListModel(parent)
{
    // Make sure we can fit 4 hours without resizing
    states_.reserve(4 * 3600);
}

StateModel::~StateModel()
{

}

int StateModel::count() const
{
    return states_.size();
}

int StateModel::lastIndex() const
{
    return count() ? count() - 1 : 0;
}

quint32 StateModel::lastSecond() const
{
    return (quint32)lastIndex();
}

int StateModel::indexOf(int second)
{
    return count() <= second ? -1 : second;
}

QString StateModel::stepName(QString step)
{
    if (step == "I") return "Idle";
    else if (step == "M0") return "Mash #1 start";
    else if (step == "M1") return "Mash #2 start";
    else if (step == "M2") return "Mash #3 start";
    else if (step == "M3") return "Mash #4 start";
    else if (step == "B0") return "Boil start";
    else if (step == "B1") return "Boil reached";
    else if (step == "B2") return "Hop #1 dropped";
    else if (step == "B3") return "Hop #2 dropped";
    else if (step == "C") return "Cooler start";
    else return "Unknown";
}

int StateModel::stepIndex(QString step)
{
    QMap<QString,int>::const_iterator it = stepIndex_.find(step);
    if (it == stepIndex_.end())
        return -1;
    return it.value();
}

void StateModel::add(StateItem *state)
{
    if (!state)
        return;

    //qDebug() << Q_FUNC_INFO;

    if (count() && state->time() <= lastSecond())
        return; // ignore duplicate time entry

    qDebug() << Q_FUNC_INFO << "state->time:" << state->time() << "lastSecond:" << lastSecond();

    state = new StateItem(*state);
    QQmlEngine::setObjectOwnership(state, QQmlEngine::CppOwnership);

    // DEBUG>>
#if 0
    if (count() >= 2)
        state->setProgramStep("M0");
    if (count() >= 10)
        state->setProgramStep("M1");
    if (count() >= 18)
        state->setProgramStep("M2");
    if (count() >= 26)
        state->setProgramStep("M3");
    if (count() >= 34)
        state->setProgramStep("B0");
    if (count() >= 42)
        state->setProgramStep("B1");
    if (count() >= 50)
        state->setProgramStep("B2");
    if (count() >= 58)
        state->setProgramStep("B3");
    if (count() >= 66)
        state->setProgramStep("C");
    if (count() >= 74)
        state->setProgramStep("I");
#endif
    // <<DEBUG

    StateItem *prevState = count() ? states_[count() - 1] : NULL;
    QString prevStep = prevState ? prevState->programStep() : QString("I");
    QString crtStep = state->programStep();

    // Add in-between seconds if missing
    quint32 time = prevState ? prevState->time() + 1 : 0;
    while (time < state->time()) {
        StateItem *extraState = prevState ? new StateItem(prevState) : new StateItem(state->parent());
        extraState->setTime(time);

        qDebug() << Q_FUNC_INFO << "adding in-between state, time:" << extraState->time();
        QQmlEngine::setObjectOwnership(extraState, QQmlEngine::CppOwnership);
        states_.append(extraState);

        ++time;
    }

    states_.append(state);

    if (crtStep != prevStep) {
        stepIndex_[crtStep] = lastIndex();
        qDebug() << Q_FUNC_INFO << "start of program step:" << crtStep;
    }

    emit newData();
}

StateItem *StateModel::get(int index)
{
    //qDebug() << Q_FUNC_INFO << "index" << index << "count" << count();

    if (index >= count() || index < 0)
        return NULL;

    return states_[index];
}

int StateModel::rowCount(const QModelIndex &parent) const
{
    return !parent.isValid() ? count() : 0;
}

QVariant StateModel::data(const QModelIndex &index, int /*role*/) const
{
    int row = index.row();

    //qDebug() << Q_FUNC_INFO << "row" << row << "count" << count();

    if (row >= count() || row < 0)
        return QVariant();

    return QVariant::fromValue(states_.at(row).data());
}


QModelIndex StateModel::index(int row, int column, const QModelIndex &parent) const
{
    return row >= 0 && row < count() && column == 0 && !parent.isValid()
            ? createIndex(row, column)
            : QModelIndex();
}


QDataStream &operator<<(QDataStream &out, const StateItem &item)
{
    out << item.m_code <<
           item.m_running <<
           item.m_time <<
           item.m_programStep <<
           item.m_temp1 <<
           item.m_temp2 <<
           item.m_pump1On <<
           item.m_pump1Flow <<
           item.m_pump2AndCoolerValveOn <<
           item.m_pump2Flow <<
           item.m_heaterOn;
    return out;
}


QDataStream &operator>>(QDataStream &in, StateItem &item)
{
    in >> item.m_code >>
          item.m_running >>
          item.m_time >>
          item.m_programStep >>
          item.m_temp1 >>
          item.m_temp2 >>
          item.m_pump1On >>
          item.m_pump1Flow >>
          item.m_pump2AndCoolerValveOn >>
          item.m_pump2Flow >>
          item.m_heaterOn;
    return in;
}
