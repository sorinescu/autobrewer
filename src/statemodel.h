#ifndef STATEMODEL_H
#define STATEMODEL_H

#include <QAbstractListModel>
#include <QPointer>
#include <QVector>
#include <QMap>
#include <qdebug.h>

struct StateItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(quint32 code READ code WRITE setCode NOTIFY codeChanged)
    Q_PROPERTY(bool running READ running WRITE setRunning NOTIFY runningChanged)
    Q_PROPERTY(quint32 time READ time WRITE setTime NOTIFY timeChanged)
    Q_PROPERTY(QString program_step READ programStep WRITE setProgramStep NOTIFY programStepChanged)
    Q_PROPERTY(float temp1 READ temp1 WRITE setTemp1 NOTIFY temp1Changed)
    Q_PROPERTY(float temp2 READ temp2 WRITE setTemp2 NOTIFY temp2Changed)
    Q_PROPERTY(bool pump1_on READ pump1On WRITE setPump1On NOTIFY pump1OnChanged)
    Q_PROPERTY(bool pump2_and_cooler_valve_on READ pump2AndCoolerValveOn WRITE setPump2AndCoolerValveOn NOTIFY pump2AndCoolerValveOnChanged)
    Q_PROPERTY(float pump1_flow READ pump1Flow WRITE setPump1Flow NOTIFY pump1FlowChanged)
    Q_PROPERTY(float pump2_flow READ pump2Flow WRITE setPump2Flow NOTIFY pump2FlowChanged)
    Q_PROPERTY(bool heater_on READ heaterOn WRITE setHeaterOn NOTIFY heaterOnChanged)
    Q_PROPERTY(bool hop1_deploy_on READ hop1DeployOn WRITE setHop1DeployOn NOTIFY hop1DeployOnChanged)
    Q_PROPERTY(bool hop2_deploy_on READ hop2DeployOn WRITE setHop2DeployOn NOTIFY hop2DeployOnChanged)
    Q_PROPERTY(quint32 heater_triac_fan_speed READ heaterTriacFanSpeed WRITE setHeaterTriacFanSpeed NOTIFY heaterTriacFanSpeedChanged)
    Q_PROPERTY(QString majorStepName READ majorStepName NOTIFY majorStepNameChanged)

    quint32 m_code;                     // Error code
    bool m_running;                 // Is brew program running ?
    quint32 m_time;                     // Elapsed time since program start, in seconds
    QString m_programStep;          // see stepName()
    float m_temp1;                  // Temperature near heater, in Celsius
    float m_temp2;                  // Temperature in mash bucket, in Celsius
    bool m_pump1On;                 // Mashing pump on ?
    bool m_pump2AndCoolerValveOn;   // Cooling pump on ?
    float m_pump1Flow;              // Mashing pump flow, in liters/min
    float m_pump2Flow;              // Cooling pump flow, in liters/min
    bool m_heaterOn;                // Heating element on ?
    bool m_hop1DeployOn;            // Hop #1 deploy coil on ?
    bool m_hop2DeployOn;            // Hop #2 deploy coil on ?
    quint32 m_heaterTriacFanSpeed;      // Triac fan speed, in RPM

    friend QDataStream &operator<<(QDataStream &out, const StateItem &item);
    friend QDataStream &operator>>(QDataStream &in, StateItem &item);

public:
    explicit StateItem(QObject *parent = 0)
        : QObject(parent),
          m_code(0),
          m_running(false),
          m_time(0),
          m_programStep('I'),
          m_temp1(0),
          m_temp2(0),
          m_pump1On(false),
          m_pump2AndCoolerValveOn(false),
          m_pump1Flow(0),
          m_pump2Flow(0),
          m_heaterOn(false),
          m_hop1DeployOn(false),
          m_hop2DeployOn(false),
          m_heaterTriacFanSpeed(0)
    {
        //qDebug() << Q_FUNC_INFO;
    }

    StateItem(StateItem const& other)
            : QObject(other.parent()),
              m_code(other.m_code),
              m_running(other.m_running),
              m_time(other.m_time),
              m_programStep(other.m_programStep),
              m_temp1(other.m_temp1),
              m_temp2(other.m_temp2),
              m_pump1On(other.m_pump1On),
              m_pump2AndCoolerValveOn(other.m_pump2AndCoolerValveOn),
              m_pump1Flow(other.m_pump1Flow),
              m_pump2Flow(other.m_pump2Flow),
              m_heaterOn(other.m_heaterOn),
              m_hop1DeployOn(other.m_hop1DeployOn),
              m_hop2DeployOn(other.m_hop2DeployOn),
              m_heaterTriacFanSpeed(other.m_heaterTriacFanSpeed)
    {
        //qDebug() << Q_FUNC_INFO;
    }

    ~StateItem()
    {
        //qDebug() << Q_FUNC_INFO;
    }

    Q_INVOKABLE static QString tempToString(float temp)
    {
        if (!temp)
            return "N/A";
        return QString("%1°C").arg(temp, 0, 'f', 2);
    }

    Q_INVOKABLE static QString flowToString(float flow)
    {
        return QString("%1 l/min").arg(flow, 0, 'f', 1);
    }

    QString majorStepName()
    {
        if (m_programStep == "I") return "Idle";
        else if (m_programStep == "M0") return "Mashing, step #1";
        else if (m_programStep == "M1") return "Mashing, step #2";
        else if (m_programStep == "M2") return "Mashing, step #3";
        else if (m_programStep == "M3") return "Mashing, step #4";
        else if (m_programStep.startsWith('B')) return "Boiling";
        else if (m_programStep == "C") return "Cooling";
        else return "Unknown";
    }

    quint32 code() const
    {
        return m_code;
    }
    bool running() const
    {
        return m_running;
    }
    quint32 time() const
    {
        return m_time;
    }
    QString programStep() const
    {
        return m_programStep;
    }
    float temp1() const
    {
        return m_temp1;
    }
    float temp2() const
    {
        return m_temp2;
    }
    bool pump1On() const
    {
        return m_pump1On;
    }
    bool pump2AndCoolerValveOn() const
    {
        return m_pump2AndCoolerValveOn;
    }
    float pump1Flow() const
    {
        return m_pump1Flow;
    }
    float pump2Flow() const
    {
        return m_pump2Flow;
    }
    bool heaterOn() const
    {
        return m_heaterOn;
    }
    bool hop1DeployOn() const
    {
        return m_hop1DeployOn;
    }
    bool hop2DeployOn() const
    {
        return m_hop2DeployOn;
    }
    quint32 heaterTriacFanSpeed() const
    {
        return m_heaterTriacFanSpeed;
    }

public slots:
    void setCode(quint32 arg)
    {
        if (m_code == arg)
            return;

        m_code = arg;
        emit codeChanged(arg);
    }
    void setRunning(bool arg)
    {
        if (m_running == arg)
            return;

        m_running = arg;
        emit runningChanged(arg);
    }
    void setTime(quint32 arg)
    {
        if (m_time == arg)
            return;

        m_time = arg;
        emit timeChanged(arg);
    }
    void setProgramStep(QString arg)
    {
        if (m_programStep == arg)
            return;

        m_programStep = arg;
        emit programStepChanged(arg);
        emit majorStepNameChanged(majorStepName());
    }
    void setTemp1(float arg)
    {
        if (m_temp1 == arg)
            return;

        m_temp1 = arg;
        emit temp1Changed(arg);
    }
    void setTemp2(float arg)
    {
        if (m_temp2 == arg)
            return;

        m_temp2 = arg;
        emit temp2Changed(arg);
    }
    void setPump1On(bool arg)
    {
        if (m_pump1On == arg)
            return;

        m_pump1On = arg;
        emit pump1OnChanged(arg);
    }
    void setPump2AndCoolerValveOn(bool arg)
    {
        if (m_pump2AndCoolerValveOn == arg)
            return;

        m_pump2AndCoolerValveOn = arg;
        emit pump2AndCoolerValveOnChanged(arg);
    }
    void setPump1Flow(float arg)
    {
        if (m_pump1Flow == arg)
            return;

        m_pump1Flow = arg;
        emit pump1FlowChanged(arg);
    }
    void setPump2Flow(float arg)
    {
        if (m_pump2Flow == arg)
            return;

        m_pump2Flow = arg;
        emit pump2FlowChanged(arg);
    }
    void setHeaterOn(bool arg)
    {
        if (m_heaterOn == arg)
            return;

        m_heaterOn = arg;
        emit heaterOnChanged(arg);
    }
    void setHop1DeployOn(bool arg)
    {
        if (m_hop1DeployOn == arg)
            return;

        m_hop1DeployOn = arg;
        emit hop1DeployOnChanged(arg);
    }
    void setHop2DeployOn(bool arg)
    {
        if (m_hop2DeployOn == arg)
            return;

        m_hop2DeployOn = arg;
        emit hop2DeployOnChanged(arg);
    }
    void setHeaterTriacFanSpeed(quint32 arg)
    {
        if (m_heaterTriacFanSpeed == arg)
            return;

        m_heaterTriacFanSpeed = arg;
        emit heaterTriacFanSpeedChanged(arg);
    }

signals:
    void codeChanged(quint32 arg);
    void runningChanged(bool arg);
    void timeChanged(quint32 arg);
    void programStepChanged(QString arg);
    void temp1Changed(float arg);
    void temp2Changed(float arg);
    void pump1OnChanged(bool arg);
    void pump2AndCoolerValveOnChanged(bool arg);
    void pump1FlowChanged(float arg);
    void pump2FlowChanged(float arg);
    void heaterOnChanged(bool arg);
    void hop1DeployOnChanged(bool arg);
    void hop2DeployOnChanged(bool arg);
    void heaterTriacFanSpeedChanged(quint32 arg);
    void majorStepNameChanged(QString arg);
};

Q_DECLARE_METATYPE(StateItem)

QDataStream &operator<<(QDataStream &out, const StateItem &item);
QDataStream &operator>>(QDataStream &in, StateItem &item);

class StateModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(int lastSecond READ lastSecond NOTIFY lastSecondChanged)
    Q_PROPERTY(int lastIndex READ lastIndex NOTIFY lastIndexChanged)

    QVector<QPointer<StateItem> > states_;
    QMap<QString,int> stepIndex_;

public:
    explicit StateModel(QObject *parent = 0);
    ~StateModel();

    int count() const;
    int lastIndex() const;
    quint32 lastSecond() const;

    Q_INVOKABLE int indexOf(int second);
    Q_INVOKABLE static QString stepName(QString step);
    Q_INVOKABLE int stepIndex(QString step);

    Q_INVOKABLE void add(StateItem* state);
    Q_INVOKABLE StateItem * get(int index);

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QModelIndex index(int row, int column, const QModelIndex &parent) const;

signals:
    void countChanged(int count);
    void lastSecondChanged(int lastSecond);
    void lastIndexChanged(int lastIndex);
    void newData();
};

#endif // STATEMODEL_H
