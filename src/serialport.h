#ifndef SERIALPORT_H
#define SERIALPORT_H

#include "platformdetails.h"

#include <QSerialPort>
#include <QQmlListProperty>
#include <qdebug.h>

class SerialPortInfo : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString path READ path)
    Q_PROPERTY(QString name READ name)

    QString m_path;
    QString m_name;

public:
    explicit SerialPortInfo(QObject *parent = 0) : QObject(parent) { }
    ~SerialPortInfo() { }

    QString path() const
    {
        return m_path;
    }

    void setPath(QString arg)
    {
        m_path = arg;
    }

    QString name() const
    {
        return m_name;
    }

    void setName(QString arg)
    {
        m_name = arg;
    }
};

class SerialPort : public QSerialPort
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<SerialPortInfo> availablePorts READ availablePorts)
    Q_PROPERTY(bool connected READ isOpen NOTIFY connectedChanged)

public:
    explicit SerialPort(QObject *parent = 0);
    ~SerialPort();

    QQmlListProperty<SerialPortInfo> availablePorts();
    Q_INVOKABLE void connect(QString device);
    Q_INVOKABLE void reconnect();
    Q_INVOKABLE void disconnect();
    Q_INVOKABLE void send(QString data);

    static int portInfoCount(QQmlListProperty<SerialPortInfo> *list) {
        //qDebug() << Q_FUNC_INFO;
        QList<SerialPortInfo*> *ports = static_cast<QList<SerialPortInfo*> *>(list->data);
        return ports->count();
    }

    static SerialPortInfo* portInfoAt(QQmlListProperty<SerialPortInfo> *list, int index) {
        //qDebug() << Q_FUNC_INFO;
        QList<SerialPortInfo*> *ports = static_cast<QList<SerialPortInfo*> *>(list->data);
        return ports->at(index);
    }

signals:
    void dataReceived(QString data);
    void errorSignal(QString message);

    void connectedChanged(bool arg);

private slots:
    void readAvailableData();
    void handleError(QSerialPort::SerialPortError error);

protected:
    QList<SerialPortInfo*> m_ports;
    QString m_deviceName;
    QString m_line;

    static QString errorStr(QSerialPort::SerialPortError error);
};

#endif // SERIALPORT_H
