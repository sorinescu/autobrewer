#include "mybluetoothdevice.h"

MyBluetoothDevice::MyBluetoothDevice(QObject *parent) : QBluetoothLocalDevice(parent)
{
    connect(this, SIGNAL(hostModeStateChanged(QBluetoothLocalDevice::HostMode)),
            this, SLOT(onHostModeStateChanged(QBluetoothLocalDevice::HostMode)));
}

MyBluetoothDevice::~MyBluetoothDevice()
{
}

void MyBluetoothDevice::setHostMode(QBluetoothLocalDevice::HostMode mode)
{
    if (QBluetoothLocalDevice::hostMode() == HostPoweredOff && mode != HostPoweredOff) {
        QBluetoothLocalDevice::setHostMode(mode);
        powerOn();
    }
    else
        QBluetoothLocalDevice::setHostMode(mode);
}

void MyBluetoothDevice::onHostModeStateChanged(QBluetoothLocalDevice::HostMode state)
{
    hostModeChanged(state);
}
