#include "svglayerpixmapprovider.h"

#include <QPainter>
#include <QSvgRenderer>

SvgLayerPixmapProvider::SvgLayerPixmapProvider()
    : QQuickImageProvider(QQuickImageProvider::Pixmap)
{
}

SvgLayerPixmapProvider::~SvgLayerPixmapProvider()
{
}

QPixmap SvgLayerPixmapProvider::requestPixmap(const QString &id, QSize *size, const QSize &requestedSize)
{
    int start = id.indexOf('&');
    QSvgRenderer svgRenderer(id.left(start));   // -1 means the full string

    int width = requestedSize.width() > 0 ? requestedSize.width() : svgRenderer.defaultSize().width();
    int height = requestedSize.height() > 0 ? requestedSize.height() : svgRenderer.defaultSize().height();

    if (size)
        *size = QSize(width, height);

    QPixmap pixmap(width, height);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);

    if (start >= 0)   // render individual layer(s)
    {
        while (start >= 0)
        {
            ++start;
            int end = id.indexOf('&', start);

            QString layer = id.mid(start, end >= 0 ? end-start : -1); // -1 means end of string
            svgRenderer.render(&painter, layer, svgRenderer.boundsOnElement(layer));

            start = end;
        }
    }
    else    // render full SVG
    {
        svgRenderer.render(&painter);
    }

    return pixmap;
}
