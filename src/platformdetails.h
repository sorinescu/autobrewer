#ifndef PLATFORMDETAILS_H
#define PLATFORMDETAILS_H

#include <QObject>

#define AUTOBREWER_HAS_SERIAL_PORT \
    ((defined(Q_OS_LINUX) && !defined(Q_OS_ANDROID)) || defined(Q_OS_WIN32) || defined(Q_OS_WINRT) || defined(Q_OS_OSX))

#define AUTOBREWER_HAS_BLUETOOTH \
    defined(Q_OS_LINUX) || defined(Q_OS_OSX) // Android is Linux too

class PlatformDetails : public QObject
{
    Q_OBJECT
    Q_PROPERTY (bool hasBluetooth READ hasBluetooth)
    Q_PROPERTY (bool hasSerialPort READ hasSerialPort)
public:
    explicit PlatformDetails(QObject *parent = 0);
    ~PlatformDetails();

    bool hasBluetooth();
    bool hasSerialPort();
signals:

public slots:
};

#endif // PLATFORMDETAILS_H
