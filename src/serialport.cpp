#include "serialport.h"

#include <QtSerialPort/QSerialPortInfo>

SerialPort::SerialPort(QObject *parent) : QSerialPort(parent)
{
    QObject::connect(this, SIGNAL(error(QSerialPort::SerialPortError)), this,
                     SLOT(handleError(QSerialPort::SerialPortError)));
    QObject::connect(this, SIGNAL(readyRead()), this, SLOT(readAvailableData()));
}

SerialPort::~SerialPort()
{
    disconnect();
}

QQmlListProperty<SerialPortInfo> SerialPort::availablePorts()
{
    QList<QSerialPortInfo> serialPortInfoList = QSerialPortInfo::availablePorts();

    m_ports.clear();
    foreach (const QSerialPortInfo &serialPortInfo, serialPortInfoList) {
        SerialPortInfo *port = new SerialPortInfo(this);
        //qDebug() << Q_FUNC_INFO << "location" << serialPortInfo.systemLocation();

        port->setName(serialPortInfo.portName());
        port->setPath(serialPortInfo.systemLocation());
        m_ports.append(port);
    }
    return QQmlListProperty<SerialPortInfo>(this, &m_ports, portInfoCount, portInfoAt);
}

#define SERIAL_CHECK_RESULT(expr) \
    if (!(expr)) { return; }

void SerialPort::connect(QString device)
{
    if (device == "")
        return;

    if (isOpen())
        close();

    clearError();
    m_deviceName = device;
    setPortName(device);

    SERIAL_CHECK_RESULT(setBaudRate(QSerialPort::Baud9600));
    SERIAL_CHECK_RESULT(setDataBits(QSerialPort::Data8));
    SERIAL_CHECK_RESULT(setFlowControl(QSerialPort::NoFlowControl));
    SERIAL_CHECK_RESULT(setParity(QSerialPort::NoParity));
    SERIAL_CHECK_RESULT(setStopBits(QSerialPort::OneStop));

    SERIAL_CHECK_RESULT(open(QIODevice::ReadWrite));

    emit connectedChanged(isOpen());
}

void SerialPort::reconnect()
{
    if (m_deviceName != "")
        connect(m_deviceName);
}

void SerialPort::disconnect()
{
    if (isOpen()) {
        close();
        emit connectedChanged(isOpen());
    }
}

void SerialPort::send(QString data)
{
    if (!isOpen())
        return;

    QByteArray line = (data + '\n').toLatin1();
    char *buf = (char *)line.data();
    qint64 total = line.length();

    while (total) {
        qint64 written = write(buf, total);

        if (written < 0) {
            emit errorSignal("Serial port write failure");
            break;
        }

        waitForBytesWritten(-1);
        total -= written;
        buf += written;
    }
}

void SerialPort::readAvailableData()
{
    QByteArray buf = readAll();
    char *start = buf.data();
    for (char *data = start; *data; ++data) {
        if (*data == '\n') {
            *data = 0;
            m_line.append(start);
            start = data + 1;

            //qDebug() << "Got line: " << m_line;
            emit dataReceived(m_line);
            m_line.clear();
        }
    }

    if (*start)
        m_line.append(start);
}

void SerialPort::handleError(QSerialPort::SerialPortError error)
{
    if (error != QSerialPort::NoError) {
        emit errorSignal(errorStr(error));
        disconnect();
    }
}

QString SerialPort::errorStr(QSerialPort::SerialPortError error)
{
    switch (error) {
    case QSerialPort::NoError: return "Success";
    case QSerialPort::DeviceNotFoundError: return "Serial device not found";
    case QSerialPort::PermissionError: return "Not allowed to access serial device";
    case QSerialPort::OpenError: return "Failed to open serial device";
    case QSerialPort::NotOpenError: return "Serial device is not opened";
    case QSerialPort::ParityError: return "Serial device parity error";
    case QSerialPort::FramingError: return "Serial device framing error";
    case QSerialPort::BreakConditionError: return "Serial device break error";
    case QSerialPort::WriteError: return "Error while writing to serial device";
    case QSerialPort::ReadError: return "Error while reading from serial device";
    case QSerialPort::ResourceError: return "Serial device removed unexpectedly";
    case QSerialPort::UnsupportedOperationError: return "Can't perform requested serial device operation";
    case QSerialPort::TimeoutError: return "Timeout while communicating with serial device";
    default: return "Unknown serial device error";
    }
}
