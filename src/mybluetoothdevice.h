#ifndef MYBLUETOOTHDEVICE_H
#define MYBLUETOOTHDEVICE_H

#include <qbluetoothlocaldevice.h>

class MyBluetoothDevice : public QBluetoothLocalDevice
{
    Q_OBJECT
    Q_PROPERTY(HostMode hostMode READ hostMode WRITE setHostMode NOTIFY hostModeChanged)
public:
    explicit MyBluetoothDevice(QObject *parent = 0);
    ~MyBluetoothDevice();

    void setHostMode(HostMode mode);

signals:
    void hostModeChanged(HostMode mode);

public slots:
    void onHostModeStateChanged(QBluetoothLocalDevice::HostMode state);
};

#endif // MYBLUETOOTHDEVICE_H
