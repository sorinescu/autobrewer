#include "svglayerpixmapprovider.h"

#include <QDir>
#include <QGuiApplication>
#include <QQmlEngine>
//#include <QQmlFileSelector>
#include <QQmlComponent>
#include <QQmlContext>
#include <QQuickWindow>
#include <QDebug>
#include "mybluetoothdevice.h"
#include "platformdetails.h"
#include "statemodel.h"

#if AUTOBREWER_HAS_SERIAL_PORT
#include "serialport.h"
#endif

static QObject *currentBrewStateProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    StateItem *state = new StateItem();
    return state;
}

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    app.setOrganizationName("sorinescu");
    app.setOrganizationDomain("sorinescu.ro");
    app.setApplicationName(QFileInfo(app.applicationFilePath()).baseName());

    QQmlEngine engine;
    engine.addImageProvider(QLatin1String("svglayer"), new SvgLayerPixmapProvider);

    qmlRegisterType<MyBluetoothDevice>("ro.sorinescu.autobrewer", 1, 0, "BluetoothDevice");
    qmlRegisterType<PlatformDetails>("ro.sorinescu.autobrewer", 1, 0, "PlatformDetails");
    qmlRegisterType<StateModel>("ro.sorinescu.autobrewer", 1, 0, "BrewStateModel");
    qmlRegisterType<StateItem>("ro.sorinescu.autobrewer", 1, 0, "BrewStateItem");
    qmlRegisterSingletonType<StateItem>("ro.sorinescu.autobrewer", 1, 0, "CurrentBrewState", currentBrewStateProvider);

#if AUTOBREWER_HAS_SERIAL_PORT
    qmlRegisterType<SerialPort>("ro.sorinescu.autobrewer", 1, 0, "SerialPort");
    qmlRegisterType<SerialPortInfo>("ro.sorinescu.autobrewer", 1, 0, "SerialPortInfo");
#endif

    // We want to save and load StateItems
    qRegisterMetaTypeStreamOperators<StateItem>();

    QQmlComponent component(&engine);
    //QQuickWindow::setDefaultAlphaBuffer(true);
    component.loadUrl(QUrl("qrc:/qml/MainWindow.qml"));
    QQuickWindow *mainWnd = qobject_cast<QQuickWindow *>(component.create());

    QIcon icon(":/images/icon.svg");
    mainWnd->setIcon(icon);

    return app.exec();
}
