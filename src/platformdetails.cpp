#include "platformdetails.h"

PlatformDetails::PlatformDetails(QObject *parent) : QObject(parent)
{

}

PlatformDetails::~PlatformDetails()
{

}

bool PlatformDetails::hasBluetooth()
{
#if AUTOBREWER_HAS_BLUETOOTH
    return true;
#else
    return false;
#endif
}

bool PlatformDetails::hasSerialPort()
{
#if AUTOBREWER_HAS_SERIAL_PORT
    return true;
#else
    return false;
#endif
}

