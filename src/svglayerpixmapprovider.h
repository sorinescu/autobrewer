#ifndef SVGLAYERIMAGEPROVIDER_H
#define SVGLAYERIMAGEPROVIDER_H

#include <QQuickImageProvider>

class SvgLayerPixmapProvider : public QQuickImageProvider
{
public:
    SvgLayerPixmapProvider();
    ~SvgLayerPixmapProvider();

    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize);
};

#endif // SVGLAYERIMAGEPROVIDER_H
