#include <EEPROM.h>
#include <OneWire.h>

// External interrupts are handled by Arduino's core code
#define EI_NOTEXTERNAL
#include <EnableInterrupt.h>

#define REQUIRESALARMS false
#define TEMPERATURE_PRECISION 9
#include <DallasTemperature.h>

#define MYDEBUG 0

#define DC_ADAPTER_MAX_CURRENT 300  // FP100, 3A

/*
 * Pin | Port  | Bit | Dir | Function
 *  0  | PORTD |  0  | in  | serial RX
 *  1  | PORTD |  1  | out | serial TX
 *  2  | PORTD |  2  | in  | pump1_flow
 *  3  | PORTD |  3  | in  | pump2_flow
 *  4  | PORTD |  4  | out | -
 *  5  | PORTD |  5  | out | heater_on
 *  6  | PORTD |  6  | out | hop1_deploy_on
 *  7  | PORTD |  7  | out | hop2_deploy_on
 *  8  | PORTB |  0  | i/o | temp1, temp2 (OneWire)
 *  9  | PORTB |  1  | in  | triac_fan
 * 10  | PORTB |  2  | out | pump1_on
 * 11  | PORTB |  3  | out | pump2_and_cooler_valve_on
 * 12  | PORTB |  4  | in  | overflow (internal pullup)
 * 13  | PORTB |  5  | out | led
 * ----------------------------------------------------
 * 17  | PORTC |  3  | out | buzzer (as digital) on A3
 */
#define ONEWIRE_PIN 8
#define PUMP1_ON_PIN 10
#define PUMP2_AND_COOLER_VALVE_ON_PIN 11
#define HEATER_ON_PIN 5
#define HOP1_DEPLOY_ON_PIN 6
#define HOP2_DEPLOY_ON_PIN 7
#define TRIAC_FAN_PIN 9
#define OVERFLOW_PIN 12
#define LED_PIN 13
#define BUZZER_PIN 17
#define BUZZER_FREQ 2000  // 2kHz

#define MAX_MASH_STEPS 4
#define LINE_BUFFER_SIZE 256

#if MYDEBUG
#define DBG_print Serial.print
#define DBG_println Serial.println
#define DBG_write Serial.write
#else
#define DBG_print(...) do {} while (0)
#define DBG_println(...) do {} while (0)
#define DBG_write(...) do {} while (0)
#endif

// Updated in loop()
volatile unsigned long time;

enum Status {
  ERR_OK,
  ERR_TOO_MUCH_DATA,
  ERR_INVALID_COMMAND,
  ERR_INVALID_FORMAT,
  ERR_PROGRAM_RUNNING,
  ERR_CURRENT_LIMIT_EXCEEDED,
  ERR_INVALID_STATE,
  ERR_HEATER_TRIAC_FAN_FAILURE,
  ERR_PUMP1_FAILURE,
  ERR_STUCK_SPARGE,
  ERR_COOLER_FAILURE,
  ERR_PUMP2_FAILURE,
  ERR_OVERFLOW,
  ERR_NO_PROGRAM
};

struct MashStep {
  unsigned temp;
  unsigned time;
};

struct Program {
  unsigned mash_steps_count;
  MashStep mash_steps[MAX_MASH_STEPS];

  unsigned boil_time;
  unsigned hops_count;
  unsigned hop_deploy_times[2];
  unsigned cool_temp;
};

struct Settings {
  DeviceAddress temp_ids[2];
  byte dirty;

  void set_temp_id(byte idx, DeviceAddress temp_id) {
    // No change; don't rewrite
    if (!memcmp(temp_ids[idx], temp_id, sizeof(DeviceAddress)))
      return;

    memcpy(temp_ids[idx], temp_id, sizeof(DeviceAddress));
    dirty = 1;
  }

  void write()
  {
    if (!dirty)
      return;

    byte *data = (byte *)this;
    int len = sizeof(Settings);

    for (int addr=0; addr<len; ++addr)
      EEPROM.write(addr, data[addr]);

    dirty = 0;
  }

  void read()
  {
    byte *data = (byte *)this;
    int len = sizeof(Settings);

    for (int addr=0; addr<len; ++addr)
      data[addr] = EEPROM.read(addr);

    dirty = 0;
  }
};

struct Sensors {
  unsigned temp1;                   // celsius x100
  unsigned temp2;                   // celsius x100
  unsigned pump1_flow;              // l/min x10
  unsigned pump2_flow;              // l/min x10
  unsigned heater_triac_fan_speed;  // RPM
  volatile bool is_overflow;

  Sensors() :
    onewire_bus(ONEWIRE_PIN),
    temp_sensors(&onewire_bus)
  { }

  void init(DeviceAddress temp_ids_[2])
  {
    byte i;

    reset();

    temp_idx[0] = temp_idx[1] = -1;  // -1 means 'sensor not present'
    memset(temp_ids, 0, 2 * sizeof(DeviceAddress));

    // The triac fan RPM circuit is open collector
    pinMode(TRIAC_FAN_PIN, INPUT_PULLUP);

    // The overflow sensor is a switch to ground
    pinMode(OVERFLOW_PIN, INPUT_PULLUP);

    temp_sensors.begin();
    temp_sensors_count = temp_sensors.getDeviceCount();

    DBG_print("Locating temp sensors...");
    DBG_print("Found ");
    DBG_print(temp_sensors_count, DEC);
    DBG_println(" devices.");

    // Find matching sensors in the saved sensor IDs
    temp_sensors_count = min(temp_sensors_count, 2);
    for (i=0; i<temp_sensors_count; ++i)
    {
      if (temp_sensors.getAddress(temp_ids[i], i))
      {
        if (!memcmp(temp_ids[i], temp_ids_[0], sizeof(DeviceAddress)))
          temp_idx[0] = i;
        else if (!memcmp(temp_ids[i], temp_ids_[1], sizeof(DeviceAddress)))
          temp_idx[1] = i;
      }
    }

    // Assign unknown sensors to incrementing indices
    for (i=0; i<temp_sensors_count; ++i)
    {
      byte j;

      // Is index 'i' assigned to temp1 or temp2 ?
      for (j=0; j<2; ++j)
        if (temp_idx[j] == i)
          break;  // yes, it's assigned

      if (j == 2)  // i is not assigned
        // Assign it to a free index
        for (j=0; j<2; ++j)
          if (temp_idx[j] < 0) {
            temp_idx[j] = i;
            break;  // yes, it's assigned
          }
    }
  }

  void reset()
  {
    pump1_freq = pump2_freq = heater_triac_fan_freq = 0;
    pump1_flow = pump2_flow = heater_triac_fan_speed = 0;
    temp1 = temp2 = 0;
    is_overflow = false;
    overflow_start = 0;
  }

  void update_values()
  {
    pump1_flow = pump1_freq * 100 / 75;
    pump2_flow = pump2_freq * 100 / 75;
    pump1_freq = pump2_freq = 0;

    // Two impulses per rotation
    heater_triac_fan_speed = heater_triac_fan_freq * 30;
    heater_triac_fan_freq = 0;

    temp1 = temp_idx[0] >= 0 ? (unsigned)(temp_sensors.getTempC(temp_ids[temp_idx[0]]) * 100) : 0;
    temp2 = temp_idx[1] >= 0 ? (unsigned)(temp_sensors.getTempC(temp_ids[temp_idx[1]]) * 100) : 0;

    // If overflow was triggered, allow 1 second to debounce sensor
    if (!is_overflow || overflow_start + 1000 >= millis())
      is_overflow = digitalRead(OVERFLOW_PIN);
  }

  void request_temps()
  {
    temp_sensors.requestTemperatures();
  }

  unsigned get_sensor_id_and_temp(byte idx, DeviceAddress id)
  {
    if (idx >= 2 || temp_idx[idx] < 0) {
      DBG_print("No ID at index "); DBG_println(idx, DEC);
      memset(id, 0, sizeof(DeviceAddress));
      return 0;
    }

    DBG_print("Valid ID at index "); DBG_print(idx, DEC); DBG_print(" mapped to "); DBG_print(temp_idx[idx], DEC); DBG_print(" temp "); DBG_println(temp_sensors.getTempC(temp_ids[temp_idx[idx]]), 2);
    memcpy(id, temp_ids[temp_idx[idx]], sizeof(DeviceAddress));
    return (unsigned)(temp_sensors.getTempC(id) * 100);
  }

private:
  volatile unsigned pump1_freq;
  volatile unsigned pump2_freq;
  volatile unsigned heater_triac_fan_freq;
  volatile unsigned long overflow_start;
  OneWire onewire_bus;
  DallasTemperature temp_sensors;
  byte temp_sensors_count;
  DeviceAddress temp_ids[2];
  char temp_idx[2];

  friend void pump1_flow_int(void);
  friend void pump2_flow_int(void);
  friend void triac_fan_int(void);
  friend void overflow_int(void);
};

struct Outputs {
  bool pump1_on;
  bool pump2_and_cooler_valve_on;
  bool heater_on;
  bool hop1_deploy_on;
  bool hop2_deploy_on;

  void init()
  {
    turn_off();
    pinMode(PUMP1_ON_PIN, OUTPUT);
    pinMode(PUMP2_AND_COOLER_VALVE_ON_PIN, OUTPUT);
    pinMode(HEATER_ON_PIN, OUTPUT);
    pinMode(HOP1_DEPLOY_ON_PIN, OUTPUT);
    pinMode(HOP2_DEPLOY_ON_PIN, OUTPUT);
  }

  void turn_off()
  {
    set_pump1_on(false);
    set_pump2_and_cooler_valve_on(false);
    set_heater_on(false);
    set_hop1_deploy_on(false);
    set_hop2_deploy_on(false);
  }

  void set(Outputs const& other)
  {
    set_pump1_on(other.pump1_on);
    set_pump2_and_cooler_valve_on(other.pump2_and_cooler_valve_on);
    set_heater_on(other.heater_on);
    set_hop1_deploy_on(other.hop1_deploy_on);
    set_hop2_deploy_on(other.hop2_deploy_on);
  }

  bool current_limit_exceeded() const
  {
    unsigned amps = 0; // FP100
    if (pump1_on)
      amps += 170;
    if (pump2_and_cooler_valve_on)
      amps += 220;
    if (heater_on)
      amps += 2;
    if (hop1_deploy_on)
      amps += 50;
    if (hop2_deploy_on)
      amps += 50;

    return amps > DC_ADAPTER_MAX_CURRENT;
  }

  void set_pump1_on(bool val) { pump1_on = val; digitalWrite(PUMP1_ON_PIN, val ? HIGH : LOW); }
  void set_pump2_and_cooler_valve_on(bool val) { pump2_and_cooler_valve_on = val; digitalWrite(PUMP2_AND_COOLER_VALVE_ON_PIN, val ? HIGH : LOW); }
  void set_heater_on(bool val) { heater_on = val; digitalWrite(HEATER_ON_PIN, val ? HIGH : LOW); }
  void set_hop1_deploy_on(bool val) { hop1_deploy_on = val; digitalWrite(HOP1_DEPLOY_ON_PIN, val ? HIGH : LOW); }
  void set_hop2_deploy_on(bool val) { hop2_deploy_on = val; digitalWrite(HOP2_DEPLOY_ON_PIN, val ? HIGH : LOW); }
};

enum CommandType {
  CMD_NONE,
  CMD_RESET = 'R',          // Simulates a reboot
  CMD_SET_PROGRAM = 'P',    // Upload a brew program
  CMD_PLAY_PAUSE = 'Y',     // Starts or pauses the brew program
  CMD_GET_CONFIG = 'Q',     // Get the global settings
  CMD_SET_CONFIG = 'C',     // Applies new settings
  CMD_GET_STATE = 'S',      // Query current state of the machine
  CMD_MANUAL_CONTROL = 'M'  // Manual control of motors
};

struct Command {
  CommandType type;

  union {
    boolean run_or_pause;
    Settings new_config;
    Program new_program;
    Outputs new_outputs;
  };
};

struct Communications {
  Command command;
  Status status;

  Communications()
  {
    reset();
  }

  void reset()
  {
    status = ERR_OK;
    command.type = CMD_NONE;
    line_len = 0;
  }

  /* Request */

  void read_available()
  {
    while (Serial.available() > 0)
    {
      byte ch = Serial.read();

      // If the received data is too long, ignore it
      if (line_len == LINE_BUFFER_SIZE)
        status = ERR_TOO_MUCH_DATA;

      if (ch == '\n')
      {
        if (!status)
        {
          line[line_len] = 0;
          parse_command();
        }

        line_len = 0;
        break;
      }
      else if (!status)
        line[line_len++] = ch;
    }
  }

  /* Response */

#define ADD_CHAR(ch) \
  do { \
    if (line_len == LINE_BUFFER_SIZE) { \
      status = ERR_TOO_MUCH_DATA; \
      return; \
    } \
    line[line_len++] = ch; \
  } while(0)

  void add_response_char(char val)
  {
    ADD_CHAR(val);
    ADD_CHAR(' ');
  }

  void add_response_char_uint(char cval, unsigned uval)
  {
    ADD_CHAR(cval);
    add_response_uint(uval);
  }

  void add_response_uint(unsigned val)
  {
    if (val == 0)
      ADD_CHAR('0');
    else
    {
      unsigned div = 10000;
      boolean found_msd = false;

      while (div)
      {
        if (val / div)
          found_msd = true;
        if (found_msd)
          ADD_CHAR('0' + val / div);

        val %= div;
        div /= 10;
      }
    }

    ADD_CHAR(' ');
  }

  void add_response_bool(bool val)
  {
    add_response_uint(val ? 1 : 0);
  }

  void add_response_onewire_id(DeviceAddress id)
  {
    static const char table[16] = {
      '0', '1', '2', '3', '4', '5', '6', '7',
      '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };

    if (line_len + 16 > LINE_BUFFER_SIZE)
    {
      status = ERR_TOO_MUCH_DATA;
      return;
    }

    for (int i=0; i<8; ++i)
    {
      line[line_len++] = table[id[i] >> 4];
      line[line_len++] = table[id[i] & 0xf];
    }

    ADD_CHAR(' ');
  }

  void send_response()
  {
    char code = command.type ? (char)command.type : 'E';
    Serial.write(code); Serial.write(' '); Serial.print(status, DEC);
    if (line_len)
    {
      line[line_len-1] = 0;  // last char is space, which we don't need
      Serial.write(' '); Serial.print(line);
    }
    Serial.write('\n');
  }

public:
  char line[LINE_BUFFER_SIZE];
  byte line_len;
  byte new_command:1;

#define EXPECT_CHAR(ch) \
  do { \
    if (line[line_len++] != ch) { \
      status = ERR_INVALID_FORMAT; \
      DBG_print("Expected '"); DBG_write(ch); DBG_print("' at "); DBG_println(line_len-1); \
      return; \
    } \
  } while(0)

#define CHECK_STATUS_AND_EAT_SPACES() \
  do { \
    if (status) \
      return; \
    eat_spaces(); \
    if (status) \
      return; \
  } while (0)

  // Eat one or more spaces.
  void eat_spaces()
  {
    EXPECT_CHAR(' ');
    while (line[line_len] == ' ')
      ++line_len;
  }

  unsigned parse_uint()
  {
    if (line[line_len] < '0' || line[line_len] > '9')
    {
      status = ERR_INVALID_FORMAT;
      DBG_print("Wrong number at"); DBG_println(line_len);
      return 0;
    }

    unsigned val = 0;
    while (line[line_len] >= '0' && line[line_len] <= '9')
      val = (val * 10) + (line[line_len++] - '0');

    return val;
  }

  bool parse_bool()
  {
    return parse_uint() != 0;
  }

  byte parse_hex()
  {
    if (!line[line_len])
    {
      status = ERR_INVALID_FORMAT;
      return 0;
    }

    char ch = line[line_len++];
    if (ch >= '0' && ch <= '9')
      return ch - '0';
    else if (ch >= 'a' && ch <= 'f')
      return ch - 'a' + 10;
    else if (ch >= 'A' && ch <= 'F')
      return ch - 'A' + 10;
    else
    {
      status = ERR_INVALID_FORMAT;
      DBG_println("Invalid hex digit");
      return 0;
    }
  }

  void parse_onewire_id(DeviceAddress id)
  {
    for (int i=0; i<8; ++i)
    {
      byte digit = parse_hex() << 4;
      if (status)
        return;
      digit += parse_hex();
      if (status)
        return;

      id[i] = digit;
    }
  }

  void parse_command()
  {
    command.type = (CommandType)line[0];
    line_len = 1; // consume command code

    switch (command.type)
    {
      case CMD_RESET:
      case CMD_GET_CONFIG:
      case CMD_GET_STATE:
        break;
      case CMD_SET_PROGRAM:
        parse_program();
        break;
      case CMD_PLAY_PAUSE:
        parse_play_pause();
        break;
      case CMD_SET_CONFIG:
        parse_set_config();
        break;
      case CMD_MANUAL_CONTROL:
        parse_manual_control();
        break;
      default:
        status = ERR_INVALID_COMMAND;
        break;
    }

    // Paranoia
    if (status)
      command.type = CMD_NONE;
  }

  void parse_program()
  {
    /**
     * Format: P m <mash steps> b <boil_time> h [<hop_deploy1_time> [<hop_deploy2_time>]] c <cool_temp>
     * <mash steps>: <mash step 1> [<mash step 2> ...]
     *  - <mash step>: <mash_time> <mash_temp>
     *    - mash_temp is FP100
     *    - mash_time is in integer minutes
     */
    byte i;

    CHECK_STATUS_AND_EAT_SPACES();

    EXPECT_CHAR('m');
    CHECK_STATUS_AND_EAT_SPACES();

    for (i=0; i<MAX_MASH_STEPS && line[line_len] && line[line_len] != 'b'; ++i)
    {
      MashStep *step = &command.new_program.mash_steps[i];

      step->time = parse_uint() * 60;
      CHECK_STATUS_AND_EAT_SPACES();

      step->temp = parse_uint();
      CHECK_STATUS_AND_EAT_SPACES();
    }

    command.new_program.mash_steps_count = i;

    EXPECT_CHAR('b');
    CHECK_STATUS_AND_EAT_SPACES();

    command.new_program.boil_time = parse_uint() * 60;
    CHECK_STATUS_AND_EAT_SPACES();

    EXPECT_CHAR('h');
    CHECK_STATUS_AND_EAT_SPACES();

    for (i=0; i<2 && line[line_len] && line[line_len] != 'c'; ++i)
    {
      command.new_program.hop_deploy_times[i] = parse_uint() * 60;
      CHECK_STATUS_AND_EAT_SPACES();
    }
    command.new_program.hops_count = i;

    EXPECT_CHAR('c');
    CHECK_STATUS_AND_EAT_SPACES();

    command.new_program.cool_temp = parse_uint();
  }

  void parse_play_pause()
  {
    /**
     * Format: Y <play_or_pause>
     * - play_or_pause is 1 (run/continue) or 0 (pause)
     */

    CHECK_STATUS_AND_EAT_SPACES();
    command.run_or_pause = parse_uint();
  }

  void parse_set_config()
  {
    /**
     * Format: C <sensor1_id> <sensor2_id>
     * - sensor ids are the 64 bit 1Wire id of the sensor, as received from "query temp sensors" command
     */

    CHECK_STATUS_AND_EAT_SPACES();

    parse_onewire_id(command.new_config.temp_ids[0]);
    CHECK_STATUS_AND_EAT_SPACES();

    parse_onewire_id(command.new_config.temp_ids[1]);
  }

  void parse_manual_control()
  {
    /**
     * Format: M <pump1_on> <pump2_and_cooler_valve_on> <heater_on> <hop1_deploy_on> <hop2_deploy_on>
     */
    byte i;

    CHECK_STATUS_AND_EAT_SPACES();

    command.new_outputs.pump1_on = parse_bool();
    CHECK_STATUS_AND_EAT_SPACES();

    command.new_outputs.pump2_and_cooler_valve_on = parse_bool();
    CHECK_STATUS_AND_EAT_SPACES();

    command.new_outputs.heater_on = parse_bool();
    CHECK_STATUS_AND_EAT_SPACES();

    command.new_outputs.hop1_deploy_on = parse_bool();
    CHECK_STATUS_AND_EAT_SPACES();

    command.new_outputs.hop2_deploy_on = parse_bool();
  }
};

enum ProgramStep {
  RUN_IDLE,
  RUN_MASH = 'M',
  RUN_BOIL = 'B',
  RUN_COOL = 'C'
};

enum BoilingStep {
  BOIL_PREBOIL,        // before boiling point
  BOIL_REACHED,        // boiling point reached
  BOIL_HOP1_DEPLOYED,  // after hop1 deployed
  BOIL_HOP2_DEPLOYED   // after hop2 deployed
};

struct ProgramRunner {
  Status status;
  Program program;
  ProgramStep step;
  unsigned elapsed_time;

  union {
    byte mash_step;            // index of current mashing step
    BoilingStep boil_step;
  };

  ProgramRunner(Sensors& asensors, Outputs& aoutputs):
    sensors(asensors), outputs(aoutputs)
  {
    reset();
  }

  void a_second_passed()
  {
    if (is_running())
      ++elapsed_time;
  }

  boolean is_idle() const
  {
    return step == RUN_IDLE;
  }

  boolean is_running() const
  {
    return step != RUN_IDLE && running;
  }

  void reset()
  {
    status = ERR_OK;
    step = RUN_IDLE;
    running = 0;
    elapsed_time = 0;
    mash_step = 0;
    //boil_step = BOIL_PREBOIL;
    step_start_time = 0;
    mash_temp_reached = 0;
    pump_elapsed_time = 0;
    boil_temp = boil_start_time = 0;
    has_program = 0;
    memset(&program, 0, sizeof(program));

    outputs.turn_off();
  }

  void set_program(Program const& aprogram)
  {
    if (step != RUN_IDLE)
    {
      status = ERR_PROGRAM_RUNNING;
      return;
    }

    reset();
    program = aprogram;
    has_program = 1;
  }

  void start()
  {
    if (is_running())
      return;

    if (!has_program)
    {
      status = ERR_NO_PROGRAM;
      return;
    }

    status = ERR_OK;
    running = 1;
  }

  void pause()
  {
    // We don't care about error codes or current state.
    // We just turn everything off. All outputs will be set up correctly
    // after a successful start().
    running = 0;
    outputs.turn_off();
  }

  void run_state_machine()
  {
    if (status || !check_hardware())
    {
      pause();
      return;
    }

    if (!running)
      return;

    switch (step) {
    case RUN_IDLE:
      if (has_program)
        step = RUN_MASH;
      break;
    case RUN_MASH:
      run_mash();
      break;
    case RUN_BOIL:
      run_boil();
      break;
    case RUN_COOL:
      run_cool();
      break;
    default:
      status = ERR_INVALID_STATE;
      break;
    }
  }

protected:
  Sensors &sensors;
  Outputs &outputs;
  unsigned step_start_time;   // shared by all program/boil steps
  unsigned pump_elapsed_time; // shared by pumps 1 and 2 because they can't be on at the same time
  unsigned boil_temp;
  unsigned boil_start_time;
  byte has_program:1;
  byte running:1;
  byte mash_temp_reached:1;

  void run_mash()
  {
    /*
     * For each mash step:
     * - heat up the wort close to desired mash temp
     * - start mash circulation pump
     * - after mash temp is reached, start countdown
     * - after countdown is done, proceed to next mash step or boil
     */
    MashStep *mstep = &program.mash_steps[mash_step];

    // Have we kept the mash temp for the required time ?
    if (mash_temp_reached && elapsed_time - step_start_time >= mstep->time)
    {
      mash_temp_reached = 0;
      set_pump1_on(false);

      // If this was the final mash step, proceed to boil
      if (++mash_step == program.mash_steps_count)
      {
        boil_step = BOIL_PREBOIL;
        step = RUN_BOIL;
      }

      return;
    }

    // Start pump if temp is less than 0.5C from target temp
    if (sensors.temp1 + 50 >= mstep->temp)
      set_pump1_on(true);
    else  // too cold, will have to turn on heater
      set_heater_on(true);

    // We can heat the mash at most 0.5C above target temp, after that we
    // have to turn the heater off
    if (sensors.temp1 >= mstep->temp + 50)
    {
      set_heater_on(false);

      // If it get 2C above desired temp, the heater triac has probably failed
      // and the heater is always on
      if (sensors.temp1 >= mstep->temp + 200)
      {
        DBG_print("Temp too high: "); DBG_print(sensors.temp1, DEC); DBG_print(", wanted: "); DBG_println(mstep->temp + 200, DEC);
        status = ERR_HEATER_TRIAC_FAN_FAILURE;
        return;
      }
    }

    if (!mash_temp_reached)
    {
      if (sensors.temp1 >= mstep->temp)
      {
        mash_temp_reached = 1;
        step_start_time = elapsed_time;
      }
    }
  }

  void run_boil()
  {
    /*
     * The steps:
     * - heat up the mash until it reaches boiling temp (BOIL_REACHED)
     * - wait for hop1_deploy_time and drop the first hops (BOIL_HOP1_DEPLOYED)
     * - wait for hop2_deploy_time and drop the second hops (BOIL_HOP2_DEPLOYED)
     * - wait for end of boil and start cooling
     */

    // Make sure the heater is on all the time
    set_heater_on(true);

    switch (boil_step)
    {
    case BOIL_PREBOIL:
      // Record the highest temp and the time when it was reached.
      // If the max doesn't change for 30 seconds, that's the boiling point.
      if (sensors.temp1 > boil_temp)
      {
        boil_temp = sensors.temp1;
        boil_start_time = elapsed_time;
      }
      if (elapsed_time - boil_start_time >= 30)
        boil_step = BOIL_REACHED;

      break;

    case BOIL_REACHED:
      if (program.hops_count) // we have hops
      {
        if (elapsed_time - boil_start_time >= program.hop_deploy_times[0])
        {
          outputs.set_hop1_deploy_on(true);
          step_start_time = elapsed_time;
          boil_step = BOIL_HOP1_DEPLOYED;
        }
      }
      else  // no hops, no hope
        goto _end_boil;
      break;

    case BOIL_HOP1_DEPLOYED:
      // Keep the electric latch open for 2 seconds to allow hops to fall
      if (elapsed_time - step_start_time >= 2)
        outputs.set_hop1_deploy_on(false);

      // We have a second hop deployment to do
      if (program.hops_count > 1)
      {
        if (elapsed_time - boil_start_time >= program.hop_deploy_times[1])
        {
          outputs.set_hop2_deploy_on(true);
          step_start_time = elapsed_time;
          boil_step = BOIL_HOP2_DEPLOYED;
        }
      }
      else  // no secondary hops
        goto _end_boil;
      break;

    case BOIL_HOP2_DEPLOYED:
      // Keep the electric latch open for 2 seconds to allow hops to fall
      if (elapsed_time - step_start_time >= 2)
        outputs.set_hop2_deploy_on(false);

_end_boil:
      if (elapsed_time - boil_start_time >= program.boil_time)
      {
        outputs.set_hop1_deploy_on(false);  // paranoia
        outputs.set_hop2_deploy_on(false);  // paranoia

        set_heater_on(false);
        step_start_time = elapsed_time;
        step = RUN_COOL;
      }
      break;
    }
  }

  void run_cool()
  {
    /*
     * The steps:
     * - turn on cooler valve and whirlpool pump
     * - when the temperature drops to cool_temp, stop
     */

    set_pump2_and_cooler_valve_on(true);

    if (sensors.temp1 <= program.cool_temp)
    {
      set_pump2_and_cooler_valve_on(false);

      running = 0;
      has_program = 0;  // program is done, reset it
      step = RUN_IDLE;
    }

    // Let's assume the cooling process must finish in less than 1h
    if (elapsed_time - step_start_time >= 3600)
      status = ERR_COOLER_FAILURE;
  }

  bool check_hardware()
  {
    return
      (!outputs.heater_on || check_heater_triac_fan()) &&
      check_overflow() &&
      check_pump1_flow() &&
      check_pump2_flow();
  }

  bool check_heater_triac_fan()
  {
    if (sensors.heater_triac_fan_speed > 1000)
      return true;

    status = ERR_HEATER_TRIAC_FAN_FAILURE;
    return false;
  }

  bool check_overflow()
  {
    if (sensors.is_overflow)
    {
      set_pump1_on(false);
      status = ERR_OVERFLOW;
      return false;
    }

    return true;
  }

  bool check_pump1_flow()
  {
    // Allow 2 seconds of delay before checking flow.
    // This is because it takes 1 sec to compute the first flow value.
    if (pump_elapsed_time == 0 || elapsed_time - pump_elapsed_time < 2)
      return true;

    if (outputs.pump1_on)
    {
      if (sensors.pump1_flow < 10)   // 1 l/min
      {
        status = ERR_PUMP1_FAILURE;
        return false;
      }
    }
    else
    {
      // After we stop the mash circulation pump, the wort must drain from
      // the upper vessel through the pump flow meter back into the lower
      // vessel.
      // If the flow stops too quickly (less than 1 min), the sparge is stuck.
      if (elapsed_time - pump_elapsed_time >= 60 && sensors.pump1_flow < 10)
      {
        status = ERR_STUCK_SPARGE;
        return false;
      }
    }

    return true;
  }

  bool check_pump2_flow()
  {
    // Allow 2 seconds of delay before checking flow.
    // This is because it takes 1 sec to compute the first flow value.
    if (pump_elapsed_time == 0 || elapsed_time - pump_elapsed_time < 2)
      return true;

    if (outputs.pump2_and_cooler_valve_on && sensors.pump2_flow < 10)    // 1 l/min
    {
      status = ERR_PUMP2_FAILURE;
      return false;
    }

    return true;
  }

  void set_pump1_on(bool value)
  {
    // If we turn it on, the mash pot will spill over
    if (value && sensors.is_overflow)
      return;

    // Store the moment when the pump was turned on or off
    if (value != outputs.pump1_on)
      pump_elapsed_time = elapsed_time;
    outputs.set_pump1_on(value);
  }

  void set_pump2_and_cooler_valve_on(bool value)
  {
    // Store the moment when the pump was turned on or off
    if (value != outputs.pump2_and_cooler_valve_on)
      pump_elapsed_time = elapsed_time;
    outputs.set_pump2_and_cooler_valve_on(value);
  }

  void set_heater_on(bool value)
  {
    outputs.set_heater_on(value && check_heater_triac_fan());
  }
};

/*
 * Led has the following patterns, depending on program state:
 * idle:            on
 * program running: 50% duty cycle, 1s period
 * program paused:  25% duty cycle, 2s period
 * error:           50% duty cycle, 500ms period
 */
struct Led {
  Led(ProgramRunner const& program_runner) : runner(program_runner) { }

  void init()
  {
    pinMode(LED_PIN, OUTPUT);
  }

  void update()
  {
    // 2 seconds in 250ms intervals
    const byte patterns[4] = {
      0b11111111,   // idle
      0b00110011,   // running
      0b00000011,   // paused
      0b01010101    // error
    };

    byte pattern_bit = (time / 250) & 7;
    byte pattern = 0;

    if (runner.status == ERR_OK)
    {
      if (runner.is_idle())
        pattern = 0;
      else
        pattern = runner.is_running() ? 1 : 2;
    }
    else
      pattern = 3;

    digitalWrite(LED_PIN, (patterns[pattern] & (1 << pattern_bit)) ? HIGH : LOW);
  }

private:
  ProgramRunner const& runner;
};

struct Buzzer
{
  Buzzer(ProgramRunner const& program_runner) : runner(program_runner) { }

  void update()
  {
    if (runner.status == ERR_OK)
      noTone(BUZZER_PIN);
    else
    {
      if ((time / 500) & 1)
        tone(BUZZER_PIN, BUZZER_FREQ);
      else
        noTone(BUZZER_PIN);
    }
  }

private:
  ProgramRunner const& runner;
};

Settings settings;
Sensors sensors;
Outputs outputs;
Communications comm;
ProgramRunner runner(sensors, outputs);
Led led(runner);
Buzzer buzzer(runner);
unsigned long start_of_second;
boolean temp_conversion_started;

void pump1_flow_int()
{
  ++sensors.pump1_freq;
}

void pump2_flow_int()
{
  ++sensors.pump2_freq;
}

void triac_fan_int(void)
{
  ++sensors.heater_triac_fan_freq;
}

void overflow_int(void)
{
  // The mash pot is overflowing, turn off circulation pump
  sensors.is_overflow = true;
  outputs.set_pump1_on(false);
  sensors.overflow_start = time;  // allow at least 1 second to debounce
}

void setup()
{
  // start serial port at 9600 bps:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  // Set up external interrupts for the two flowmeters
  attachInterrupt(0, pump1_flow_int, RISING);
  attachInterrupt(1, pump2_flow_int, RISING);

  // Set up interrupts on pin change on triac pin and overflow sensor
  enableInterrupt(TRIAC_FAN_PIN, triac_fan_int, FALLING);
  enableInterrupt(OVERFLOW_PIN, overflow_int, RISING);

  interrupts();  // enable interrupts

  settings.read();
  sensors.init(settings.temp_ids);
  outputs.init();
  led.init();

  // Make sure we update the stored temp IDs if the physical sensors have changed
  DeviceAddress temp_id;
  sensors.get_sensor_id_and_temp(0, temp_id);
  settings.set_temp_id(0, temp_id);
  sensors.get_sensor_id_and_temp(1, temp_id);
  settings.set_temp_id(1, temp_id);
  settings.write();

  start_of_second = millis();
}

static void process_comm_command(void)
{
  if (comm.command.type == CMD_SET_PROGRAM)
  {
    runner.set_program(comm.command.new_program);
    comm.status = runner.status;
#if MYDEBUG
    byte i;
    DBG_println("Got program:");
    for (i=0; i<runner.program.mash_steps_count; ++i)
    {
      DBG_print("Mash step #"); DBG_println(i+1, DEC);
      DBG_print("  Time: "); DBG_println(runner.program.mash_steps[i].time);
      DBG_print("  Temp: "); DBG_println(runner.program.mash_steps[i].temp);
    }
    DBG_print("Boil time: "); DBG_println(runner.program.boil_time);
    for (i=0; i<runner.program.hops_count; ++i)
    {
      DBG_print("Hop #"); DBG_print(i+1, DEC); DBG_print(" deploy time: "); DBG_println(runner.program.hop_deploy_times[i]);
    }
    DBG_print("Cool to temp: "); DBG_println(runner.program.cool_temp);
#endif
  }
  else if (comm.command.type == CMD_SET_CONFIG)
  {
    settings = comm.command.new_config;
    settings.write();
    sensors.init(settings.temp_ids);
  }
  else if (comm.command.type == CMD_GET_CONFIG)
  {
    DeviceAddress id;
    unsigned temp;

    temp = sensors.get_sensor_id_and_temp(0, id);
    comm.add_response_onewire_id(id);
    comm.add_response_uint(temp);

    temp = sensors.get_sensor_id_and_temp(1, id);
    comm.add_response_onewire_id(id);
    comm.add_response_uint(temp);
  }
  else if (comm.command.type == CMD_GET_STATE)
  {
    // Response: S <status> <running> <time> <program_step> <temp1> <temp2> <pump1_on> <pump1_flow> <pump2_and_cooler_valve_on> <pump2_flow> <heater_on> <hop1_deploy_on> <hop2_deploy_on> <heater_triac_fan_speed>
    comm.status = runner.status;
    comm.add_response_bool(runner.is_running());
    comm.add_response_uint(runner.elapsed_time);

    switch (runner.step)
    {
    case RUN_IDLE:
      comm.add_response_char('I');
      break;
    case RUN_MASH:
      comm.add_response_char_uint(RUN_MASH, runner.mash_step);
      break;
    case RUN_BOIL:
      comm.add_response_char_uint(RUN_BOIL, runner.boil_step);
      break;
    case RUN_COOL:
      comm.add_response_char('C');
      break;
    }

    comm.add_response_uint(sensors.temp1);
    comm.add_response_uint(sensors.temp2);
    comm.add_response_bool(outputs.pump1_on);
    comm.add_response_uint(sensors.pump1_flow);
    comm.add_response_bool(outputs.pump2_and_cooler_valve_on);
    comm.add_response_uint(sensors.pump2_flow);
    comm.add_response_bool(outputs.heater_on);
    comm.add_response_bool(outputs.hop1_deploy_on);
    comm.add_response_bool(outputs.hop2_deploy_on);
    comm.add_response_uint(sensors.heater_triac_fan_speed);
  }
  else if (comm.command.type == CMD_MANUAL_CONTROL)
  {
    if (!runner.is_running())
    {
      if (comm.command.new_outputs.current_limit_exceeded())
        comm.status = ERR_CURRENT_LIMIT_EXCEEDED;
      else
        outputs.set(comm.command.new_outputs);
    }
    else
      comm.status = ERR_PROGRAM_RUNNING;
  }
  else if (comm.command.type == CMD_PLAY_PAUSE)
  {
    if (comm.command.run_or_pause) {
      if (!runner.is_running() && !runner.status)
        runner.start();
    }
    else
      runner.pause();
  }
  else if (comm.command.type == CMD_RESET)
  {
    runner.reset();
    sensors.reset();
  }
}

void loop()
{
  // Every second on the half second start a temperature measurement.
  // Every second on the second compute flows and read temps.
  time = millis();
  if (!temp_conversion_started && time >= start_of_second + 500)
  {
    //DBG_println("Requesting temps");
    sensors.request_temps();
    temp_conversion_started = true;
  }
  if (time >= start_of_second + 1000)
  {
    //DBG_println("Updating sensor values");
    sensors.update_values();
    runner.a_second_passed();
    temp_conversion_started = false;
    start_of_second += 1000;
  }

  runner.run_state_machine();
  led.update();
  buzzer.update();

  comm.read_available();
  if (!comm.status)
    process_comm_command();

  /* Do we have a full command or a parsing error ? */
  if (comm.command.type || comm.status)
  {
    comm.send_response();
    comm.reset();
  }
}

