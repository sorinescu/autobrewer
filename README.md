Proiectare homebrew pot
=======================

Nume: HomeBräuer

Calcule
-------

0.45 - 0.58 kg malț uscat = 1 litru

10 litri final și 5 kg malț au nevoie de:

- 18 litri apă
- 11 litri volum malț uscat
- 21 litri volum mash
- 16 litri preboil wort

Parametri design
----------------
- maxim 10 litri mash final
- maxim 5 kg malț
- maxim 2 adăugări de hamei
- mashing tip BIAB
- 2 vase (interior + exterior)
- vas exterior volum 30 litri: h=24, r=20 cm
- vas interior volum 19 litri: h=24, r=16 cm

Piese
-----
- oală
	- oală ext D=36cm email
	- capac ext D=38cm aluminiu
	- serpentină răcire Dint= Dext=
		- serpentină
			- țeavă 10mm cupru moale (11m)
			- semiolandez 15mm - 1/2" (3)
			- țeavă 15mm cupru moale (2x2cm)
			- capac 15mm cupru (2)
			- sârmă cupru plin 2.5mm (5cm)
			- racord cromat cot 1/2" ext - 1/2" ext (2)
			- garnitură silicon 1/2" (2)
		- retur vârtej
			- țeavă 15mm cupru moale (30cm)
			- racord alamă 1/2" ext - 3/4" ext
			- racord alamă 3/4" int - 1/2" ext
			- garnitură silicon 3/4"
	- element încălzire
		- element încălzire 1900W mașină spălat: http://electric14.ro/diverse/piese-de-schimb/rezistenta-electrica-masina-spalat-manuala-1900w?limit=50#.VL-4mYqUeV4
		- șaibă 16mm (2)
		- piuliță (2)
		- garnitură silicon 3/4" (2)
	- senzor temperatură DS18B20 (2): http://www.aliexpress.com/snapshot/6422379557.html?orderId=65223528045412
	- racord oală - găleată
		- 20cm țeavă cupru moale 15mm
		- semiolandez 15mm-1/2" (2)
		- reducție 3/4" int - 1/2" ext
		- cuplă rapidă mamă 1/2" alamă: http://www.dedeman.ro/brasov/cupla-rapida-din-alama-1-2-0500100.html
		- garnitură silicon 1/2" (2)
- găleată
	- găleată inox conică Dsus=25cm Djos=20cm
	- sită inox D=19cm Selgros
	- racord alamă 3/4" int - 1/2" ext, cu partea de 3/4" retezată cu flex (sorb găleată)
	- racord alamă 1/2" int - 1/2" int
	- șaibă 1/2"
	- cuplă rapidă tată 1/2" alamă: http://www.dedeman.ro/brasov/adaptor-cupla-rapida-alama-1-2fe-0500111.html
- pompe
	- pompă 15W/12V: http://www.aliexpress.com/snapshot/6256425670.html?orderId=63924830785412
	- pompă 19.8W/12V: http://www.aliexpress.com/snapshot/6415248166.html?orderId=65150943985412
	- DE AICI
- electronică
	- circuit de control
	- mufă 8 pini mamă + tată
	- mufă 6 pini mamă + tată

Electronică
-----------
- Arduino Nano are
	- 8 pini A/D
	- 13 pini D
- necesar pini:

| Name               | In | Out | Restrictions                        |
|--------------------|----|-----|-------------------------------------|
| heater             |    | x   |                                     |
| temp1              | x  |     |                                     |
| temp2              | x  |     |                                     |
| pump1              |    | x   |                                     |
| pump2+cooler_valve |    | x   |                                     |
| pump1_flow         | x  |     | digital 2 (supports ext interrupts) |
| pump2_flow         | x  |     | digital 3 (supports ext interrupts) |
| overflow           | x  |     |                                     |
| hop1_deploy        |    | x   |                                     |
| hop2_deploy        |    | x   |                                     |
| serial TX          |    | x   |                                     |
| serial RX          | x  |     |                                     |
| bluetooth state    |    | x   |                                     |
| buzzer             |    | x   |                                     |
| status_led         |    | x   |                                     |
| TOTAL              | 6  | 10  |                                     |

Pinout mufe
-----------

| Pin # | Color  | What (mash/up)     | Dir | Amps | What (boil/down) | Dir | Amps |
|-------|--------|--------------------|-----|------|------------------|-----|------|
| 1     | BLACK  | GND                | P   | 3    | GND              | P   | 2    |
| 2     | red    | 5V                 | P   |      | 5V               | P   |      |
| 3     | purple | pump1              | O   | 2    | hop1_deploy      | O   | 1    |
| 4     | white  | pump2+cooler_valve | O   | 2.5  | hop2_deploy      | O   | 1    |
| 5     | yellow | pump1_flow         | I   |      | overflow         | I   |      |
| 6     | blue   | pump2_flow         | I   |      | -                |     |      |
| 7     | green  | temp1              | I   |      | temp2 (OPT)      | I   |      |
| 8     | RED    | 12V                | P   | 3    | 12V              | P   | 2    |

Protocol comunicare
-------------------

- definitions and units:
	- FP10: fixed precision x10; e.g a value of 10.2 is encoded as 102
	- FP100: fixed precision x100; e.g a value of 10.23 is encoded as 1023
	- temperatures are in degrees Celsius
	- flows are in liters/minute
- from Arduino:
	- replies to PC queries follow the convention: <PC command> <status> <data> <LF>
		- status is 0 (success) or error code
	- error codes
		- TODO: add error codes
- from PC:
	- <command> <data> <LF>
		- reset: R
		- new program: P m <mash steps> b <boil_time> h [<hop_deploy1_time> [<hop_deploy2_time>]] c <cool_temp>
			- <mash steps>: <mash step 1> [<mash step 2> ...]
				- <mash step>: <mash_time> <mash_temp>
					- mash_temp is FP100
					- mash_time is in integer minutes
				- maximum number of mashing steps is 4
			- boil_time, hop_deploy1_time and hop_deploy2_time are integer minutes
			- hop deploy times are measured from the beginning of the boil
			- cool_temp is FP100
			- Arduino responds with: P <status code>
		- play/pause program: Y <play_or_pause>
			- play_or_pause is 1 (run/continue) or 0 (pause)
			- Arduino responds with: R <status code>
		- query global settings: Q
			- Arduino responds with: Q <status> <temp_sensor1> <temp_sensor2>
				- <temp_sensor>: <sensor_id> <temp>
					- sensor_id is the 64 bit 1Wire id of the temp sensor, in hex: AABBCCDDEEFFGGHH; a value of 0 means "sensor not present"
					- temp is current sensor reading, FP100
		- configure global settings: C <sensor1_id> <sensor2_id>
			- sensor ids are the 64 bit 1Wire id of the sensor, as received from "query global settings" command
		- query status: S
			- Arduino responds with: S <status> <running> <time> <program_step> <temp1> <temp2> <pump1_on> <pump1_flow> <pump2_and_cooler_valve_on> <pump2_flow> <heater_on> <hop1_deploy_on> <hop2_deploy_on> <heater_triac_fan_speed>
				- status is 0 (everything OK) or error code
				- running is 1 (program running) or 0 (paused/stopped)
				- time is the elapsed time since start, in seconds
				- program_step:
					- idle: I
					- mashing: M<index>
						- index is the index of the mashing step, starting at 0
					- boiling: B<index>
						- index is the position in the boil:
							- 0 is before boiling point is reached
							- 1 is after boiling point is reached, but before hop_deploy1
							- 2 is after hop_deploy1 but before hop_deploy2
							- 3 is after hop_deploy2
					- cooling: C
				- temps and flows are FP100
				- heater_triac_fan_speed is integer RPM
				- the rest are bools: 0 or 1
		- manual override: M <pump1_on> <pump2_and_cooler_valve_on> <heater_on> <hop1_deploy_on> <hop2_deploy_on>
			-Arduino responds with: M <status code>

Wires
-----

| Circuit   | Color | Stripes   |
|-----------|-------|-----------|
| Arduino   | nrva  | x/--/./-. |
| FTDI      | navr  |           |
| Bluetooth | rnva  | --/x/./-. |

De făcut
--------

- legendă:
	- (): radio button
	- []: checkbox
	- _________: text edit
- state machine pe Arduino
- ecran de configurare;
	- Y "Brew log directory"
		- file/directory selector
	- "Temperature sensors" \n "Main: " <sensor1 and current temperature> \n "Mash tun: " <sensor2 and current temperature>
		- a Swap button that is visible if both sensors are present
	- Y "Connection"
		- "Serial port" () \n <selected port name or "Not configured">
			- on click, new screen with:
				- Port name: __________ (to manually enter a port)
				- Available ports:
					- list of QSerialPort enumeration
		- "Bluetooth" () \n <selected device or "Not configured">
			- on click, Bluetooth config screen
- Y ecran de editare program
	- "Mash steps" <button "Add">
		- listview:
			- "Temperature" <slider min=40.0 max=75.0 step=0.5> <current value>
			- "Time" <slider min=0.0 max=120.0 step=1.0> <current value>
			- <button "Remove">
	- "Boiling time" <slider min=60.0 max=120.0 step=1.0> <value>
	- [] "Hop #1 drop time" <slider min=0.0 max=boiling_time step=1.0> <value>
	- [] "Hop #2 drop time" <slider min=0.0 max=boiling_time step=1.0> <value>
	- must remember last used program
- ecran de comenzi manuale (butoane)
	- current state:
		- "Main temp: " <sensor1> \n "Mash tun temp: " <sensor2> \n "Mash pump flow: " <pump1> \n "Recirculation pump flow: " <pump2>
	- "Start mashing pump" / "Stop mashing pump"
	- "Start recirculation pump" / "Stop circulation pump"
	- "Turn on heater" / "Turn off heater"
	- "Turn on water" / "Turn off water"
- butoane toolbar de:
	- editare program
	- play/pause
	- comenzi manuale în pause