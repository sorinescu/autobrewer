import QtQuick 2.0
import QtBluetooth 5.2
import ro.sorinescu.autobrewer 1.0

Item {
    id: btConnection

    readonly property alias connected: socket.connected
    property bool connecting: false
    property string deviceAddress: ""
    property string serviceUuid: ""

    BluetoothDevice {
        id: btDevice
        onHostModeChanged: {
            console.log("New host mode:", mode);
            if (mode == BluetoothDevice.HostConnectable && btConnection.connecting) {
                console.log("Continuing connection");
                btConnection._do_device_discovery();
            }
        }
    }

    BluetoothDiscoveryModel {
        id: btDiscovery
        discoveryMode: BluetoothDiscoveryModel.MinimalServiceDiscovery
        running: false

        property var matchingService

        onRunningChanged: {
            console.log("Running state:", running);
            if (!running)
                btConnection._completeConnectionIfConnecting();
            // Reset matchingService to prevent extra connects (paranoia)
            matchingService = undefined;
        }
        onServiceDiscovered: {
            console.log("Found new service, UUID:", service.serviceUuid);
            if (service.serviceUuid == btConnection.serviceUuid) {
                console.log("Service UUID matches");
                matchingService = service;
            }
        }
    }

    // Public functions

    function connect(address, uuid) {
        console.log("BT connect addr:", address, "UUID:", uuid);

        if ((socket.connected || connecting) && (address == deviceAddress && serviceUuid == uuid))
            return;

        deviceAddress = address;
        serviceUuid = uuid;

        console.log("BT connect proceed");
        connecting = true;
        socket.connected = false;

        // Power on device, if necessary
        if (btDevice.hostMode != BluetoothDevice.HostConnectable)
            btDevice.hostMode = BluetoothDevice.HostConnectable;
        else
            _do_device_discovery();
    }

    function reconnect() {
        if (connecting || socket.connected)
            return;

        connect(deviceAddress, serviceUuid);
    }

    function disconnect() {
        deviceAddress = "";
        serviceUuid = "";
        btDiscovery.running = false;
        connecting = false;
        socket.connected = false;
    }

    function send(data) {
        //console.log("BT send socket state:", socket.socketState, "connected:", BluetoothSocket.Connected);
        if (socket.socketState === BluetoothSocket.Connected) {
            //console.log("sending:", data);
            socket.stringData = data;
        }
    }

    signal dataReceived(string data)
    signal errorSignal(string message)

    // Private functions

    function _errorStr() {
        switch (socket.error) {
        case BluetoothSocket.NoError: return "No error";
        case BluetoothSocket.HostNotFoundError: return "Peer not found";
        case BluetoothSocket.ServiceNotFoundError: return "Service not found";
        case BluetoothSocket.NetworkError: return "Network error";
        case BluetoothSocket.UnsupportedProtocolError: return "Unsupported protocol";
        default: return "Unknown error";
        }
    }

    function _do_device_discovery() {
        // Make sure our device can be found.
        // We connect to it once we've discovered its service.
        console.log("Running device discovery before connection")
        btDiscovery.remoteAddress = deviceAddress;
        btDiscovery.uuidFilter = serviceUuid;
        btDiscovery.running = true;
    }

    function _completeConnectionIfConnecting() {
        console.log("BT connecting socket state:", socket.socketState,
                    "unconnected:", BluetoothSocket.Unconnected,
                    "closing:", BluetoothSocket.Closing);

        if (socket.socketState === BluetoothSocket.Closing)
            return;

        // The discovery has run to completion.
        // We either found our wanted service, in which case do_connect() has run,
        // or we didn't, so we can't connect.
        // Either way, we're no longer "connecting".
        if (!btDiscovery.matchingService)
            connecting = false;
        else if (connecting) {
            var service = btDiscovery.matchingService;
            console.log("Completing BT connection")
            console.log("Device address:", service.deviceAddress);
            console.log("Service UUID:", service.serviceUuid);
            socket.service = service;
            socket.connected = true;
            connecting = false;
        }
    }

    BluetoothSocket {
        id: socket
        onStringDataChanged: {
            var data = stringData;  // it's reset after reading
            //console.log("got new BT data:", data);
            dataReceived(data);
        }
        onErrorChanged: {
            btConnection.errorSignal(_errorStr());
        }
        onSocketStateChanged: {
            console.log("BT socket new state:", socketState);
            if (socketState === BluetoothSocket.Unconnected)
                btConnection._completeConnectionIfConnecting();
        }
    }
}

