import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.1
import QtQuick.Layouts 1.1
import ro.sorinescu.autobrewer 1.0
import "singletons" 1.0

GridLayout {
    columns: 2

    property bool manualAllowed: false

    Connections {
        target: BreweryController
        onCurrentStateChanged: manualAllowed = !CurrentBrewState.running
    }

    MessageDialog {
        id: confirmResetDlg
        title: "Reset controller"
        text: "Are you sure you want to reset the controller ?"
        icon: StandardIcon.Question
        standardButtons: StandardButton.Yes | StandardButton.No
        onYes: BreweryController.sendReset()
        onNo: console.log("Reset rejected")
    }

    BigText { text: "Lower vessel temp:"; Layout.fillWidth: true }
    BigText { text: CurrentBrewState.tempToString(CurrentBrewState.temp1) }
    BigText { text: "Upper vessel temp:"; Layout.fillWidth: true }
    BigText { text: CurrentBrewState.tempToString(CurrentBrewState.temp2) }
    BigText { text: "Mash pump flow:"; Layout.fillWidth: true }
    BigText { text: CurrentBrewState.flowToString(CurrentBrewState.pump1_flow) }
    BigText { text: "Cooling pump flow:"; Layout.fillWidth: true }
    BigText { text: CurrentBrewState.flowToString(CurrentBrewState.pump2_flow) }
    BigText { text: "Heater triac fan speed:"; Layout.fillWidth: true }
    BigText { text: CurrentBrewState.heater_triac_fan_speed + " RPM" }

    Button {
        text: "Reset"
        Layout.columnSpan: 2
        onClicked: confirmResetDlg.open()
    }

    BigText { text: "Mash pump" }
    Switch {
        id: pump1On
        enabled: manualAllowed
        onClicked: setStateField("pump1_on", checked)
        Component.onCompleted: checked = CurrentBrewState.pump1_on
        Connections {
            target: CurrentBrewState
            onPump1_onChanged: pump1On.checked = CurrentBrewState.pump1_on
        }
    }

    BigText { text: "Cooler system" }
    Switch {
        id: pump2On
        enabled: manualAllowed
        onClicked: setStateField("pump2_and_cooler_valve_on", checked)
        Component.onCompleted: checked = CurrentBrewState.pump2_and_cooler_valve_on
        Connections {
            target: CurrentBrewState
            onPump2_and_cooler_valve_onChanged: pump2On.checked = CurrentBrewState.pump2_and_cooler_valve_on
        }
    }

    BigText { text: "Heater" }
    Switch {
        id: heaterOn
        enabled: manualAllowed
        onClicked: setStateField("heater_on", checked)
        Component.onCompleted: checked = CurrentBrewState.heater_on
        Connections {
            target: CurrentBrewState
            onHeater_onChanged: heaterOn.checked = CurrentBrewState.heater_on
        }
    }

    BigText { text: "Hop #1 deploy" }
    Switch {
        id: hop1DeployOn
        enabled: manualAllowed
        onClicked: setStateField("hop1_deploy_on", checked)
        Component.onCompleted: checked = CurrentBrewState.hop1_deploy_on
        Connections {
            target: CurrentBrewState
            onHop1_deploy_onChanged: hop1DeployOn.checked = CurrentBrewState.hop1_deploy_on
        }
    }

    BigText { text: "Hop #2 deploy" }
    Switch {
        id: hop2DeployOn
        enabled: manualAllowed
        onClicked: setStateField("hop2_deploy_on", checked)
        Component.onCompleted: checked = CurrentBrewState.hop2_deploy_on
        Connections {
            target: CurrentBrewState
            onHop2_deploy_onChanged: hop2DeployOn.checked = CurrentBrewState.hop2_deploy_on
        }
    }

    function setStateField(field_name, value) {
        if (CurrentBrewState[field_name] == value)
            return;
        CurrentBrewState[field_name] = value;
        BreweryController.sendCurrentBrewState();
    }
}

