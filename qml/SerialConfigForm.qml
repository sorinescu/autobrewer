import QtQuick 2.0
import QtBluetooth 5.2
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import Qt.labs.settings 1.0
import ro.sorinescu.autobrewer 1.0

Item {
    anchors.fill: parent

    Settings {
        id: settings
        category: "serial_config"
        property string port_name
        property string port_path
    }

    property alias portName: settings.port_name
    property alias portPath: settings.port_path

    SerialPort {
        id: serialPort
    }

    Row {
        id: currentDevice
        spacing: 10

        BigText {
            font.bold: true
            text: "Current device:"
        }
        TextField {
            font.pointSize: 20
            text: portName
            focus: true
            Layout.fillWidth: true
            onTextChanged: portName = text
        }
    }

    ListModel {
        id: portsModel

        function fill() {
            clear();
            for (var i in serialPort.availablePorts) {
                var port = serialPort.availablePorts[i];
                append({ name: port.name, path: port.path });
            }
        }

        Component.onCompleted: fill()
    }

    RowLayout {
        id: availableDevicesHdr
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.top: currentDevice.bottom;
        //height: scanBtn.height

        Text {
            text: "Available devices"
            font.pointSize: 20
            font.bold: true
        }

        Item { Layout.fillWidth: true }

        Button {
            id: scanBtn
            text: "Refresh"
            onClicked: portsModel.fill()
        }
    }

    ListView {
        id: availableDevices
        anchors.top: availableDevicesHdr.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        clip: true

        model: portsModel
        delegate: Rectangle {
            id: delegate
            width: parent.width
            height: column.height + 10

            property bool expanded: true;
            clip: true

            Image {
                id: bticon
                source: "qrc:/images/serial-port.png";
                width: bttext.height;
                height: bttext.height;
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.margins: 5
            }

            Column {
                id: column
                anchors.left: bticon.right
                anchors.leftMargin: 5
                Text {
                    id: bttext
                    font.pointSize: 14
                    text: name
                }

                RowLayout
                {
                    id: details
                    visible: opacity !== 0
                    opacity: delegate.expanded ? 1 : 0.0

                    Text {
                        font.pointSize: 12
                        text: path
                    }

                    Item { Layout.fillWidth: true }

                    Button {
                        text: "Select"
                        onClicked: {
                            portName = name;
                            portPath = path;
                        }
                    }
                }
            }
        }
        focus: true
    }
}
