import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2

Rectangle {
    id: chart

    property int startSecond: 0
    property int endSecond: 120
    property bool followStateModelTail: true
    property color temp1Color: "#535154"
    property color temp2Color: "#948B3D"
    property color heaterOnColor: "#FF0000"
    property color pump1OnColor: "#FF8000"
    property color pump1FlowColor: "#994C00"
    property color pump2AndValveOnColor: "#00FFFF"
    property color pump2FlowColor: "#009999"
    property color stageStartColor: "#4C9900"
    property var stateModel: null

    property bool isPortrait: Screen.primaryOrientation == Qt.PortraitOrientation

    Component.onCompleted: {
        stateModel.newData.connect(onNewState);
    }

    function onNewState() {
        if (followStateModelTail) {
            var diff = stateModel.lastSecond - endSecond;
            if (diff >= 0) {
                startSecond += diff;
                endSecond = stateModel.lastSecond;
            }
        }

        canvas.requestPaint();
    }

    Item {
        id: canvasHolder

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.bottom: isPortrait ? colorLabels.top : parent.bottom
        anchors.right: parent.right //isPortrait ? parent.right : colorLabels.left

        PinchArea {
            anchors.fill: canvas
            anchors.leftMargin: canvas.tickMargin
            anchors.rightMargin: canvas.tickMargin
            pinch.minimumRotation: 0
            pinch.maximumRotation: 0
            pinch.minimumScale: 0.1
            pinch.maximumScale: 10

            property int beginStart;
            property int beginEnd;

            onPinchStarted: {
                beginStart = startSecond;
                beginEnd = endSecond;
            }

            onPinchUpdated: {
                var startFactor = (pinch.startCenter.x - x) / width;
                var endFactor = 1 - startFactor;
                var scale = -Math.log(pinch.scale);
                console.log(scale, startFactor, endFactor);
                updateSeconds(beginStart, beginEnd, scale, startFactor, endFactor);
            }

            function updateSeconds(beginStart, beginEnd, scale, startFactor, endFactor) {
                var nSeconds = beginEnd - beginStart;
                var newStart = beginStart - nSeconds * scale * startFactor;
                if (newStart >= 0) {
                    startSecond = newStart;
                    endSecond = Math.max(beginEnd + nSeconds * scale * endFactor, startSecond + 1);
                } else {
                    startSecond = 0;
                    endSecond = Math.max(beginEnd + nSeconds * scale, 1);
                }
                //console.log("end:", startSecond, endSecond);
            }

            MouseArea {
                id: dragArea
                hoverEnabled: true
                anchors.fill: parent
                drag.target: canvas
                onWheel: {
                    var startFactor = (mouseX - x) / width;
                    var endFactor = 1 - startFactor;
                    var scale = wheel.angleDelta.y / 120 / 10;
                    parent.updateSeconds(startSecond, endSecond, scale, startFactor, endFactor);
                }
            }
        }

        Text {
            id: fromTime
            color: "#000000"
            font.family: "Arial"
            font.pointSize: 8
            anchors.left: parent.left
            anchors.leftMargin: canvas.tickMargin
            anchors.bottom: parent.bottom
            text: "| " + ~~(startSecond/3600) + ":" + ~~(startSecond/60%60) + ":" + ~~(startSecond%60)
        }

        Text {
            id: toTime
            color: "#000000"
            font.family: "Arial"
            font.pointSize: 8
            anchors.right: parent.right
            anchors.rightMargin: canvas.tickMargin
            anchors.bottom: parent.bottom
            text: ~~(endSecond/3600) + ":" + ~~(endSecond/60%60) + ":" + ~~(endSecond%60) + " |"
        }

        Canvas {
            id: canvas

            // Uncomment below lines to use OpenGL hardware accelerated rendering.
            // See Canvas documentation for available options.
            renderTarget: Canvas.FramebufferObject
            renderStrategy: Canvas.Cooperative
            //

            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: fromTime.top

            property int tickMargin: 34
            property int activeWidth: width - 2 * tickMargin
            property int yDivisions: 10

            property real yGridOffset: 12
            property real yGridStep: (height - yGridOffset) / yDivisions

            function drawBackground(ctx) {
                ctx.save();
                ctx.fillStyle = "#ffffff";
                ctx.fillRect(0, 0, width, height);
                ctx.strokeStyle = "#d7d7d7";
                ctx.beginPath();

                // Horizontal grid lines
                for (var i = 0; i < yDivisions; i++) {
                    ctx.moveTo(0, yGridOffset + i * yGridStep);
                    ctx.lineTo(width, yGridOffset + i * yGridStep);
                }

                // Vertical grid lines
                var pixPerSecond = activeWidth / (chart.endSecond - chart.startSecond);
                var xGridStep = pixPerSecond * 60;
                var xGridOffset = pixPerSecond * (59 - startSecond % 60);
                var aHeight = 35 * height / 36;
                var yOffset = height - aHeight;
                var xOffset = tickMargin + xGridOffset;
                for (var x = 0; x < activeWidth; x += xGridStep) {
                    ctx.moveTo(xOffset + x, 0);
                    ctx.lineTo(xOffset + x, height);
                }
                ctx.stroke();

                ctx.strokeStyle = "#666666";
                ctx.beginPath();

                // Left ticks
                var xStart = 0;
                ctx.moveTo(xStart, 0);
                ctx.lineTo(xStart, height);
                for (i = 0; i < yDivisions; i++) {
                    ctx.moveTo(xStart, yGridOffset + i * yGridStep);
                    ctx.lineTo(tickMargin, yGridOffset + i * yGridStep);
                }
                ctx.stroke();

                // Right ticks
                ctx.beginPath();
                xStart = activeWidth + tickMargin;
                ctx.moveTo(xStart, 0);
                ctx.lineTo(xStart, height);
                for (i = 0; i < yDivisions; i++) {
                    ctx.moveTo(xStart, yGridOffset + i * yGridStep);
                    ctx.lineTo(width, yGridOffset + i * yGridStep);
                }
                ctx.stroke();

                // X axis
                ctx.beginPath();
                ctx.moveTo(0, yGridOffset + yDivisions * yGridStep - 1);
                ctx.lineTo(width, yGridOffset + yDivisions * yGridStep - 1);
                ctx.stroke();

                // Left Y axis
                ctx.beginPath();
                ctx.moveTo(tickMargin, 0);
                ctx.lineTo(tickMargin, height);
                ctx.stroke();

                ctx.restore();
            }

            function drawScales(ctx)
            {
                ctx.save();
                ctx.strokeStyle = "#888888";
                ctx.font = "12px Arial"
                ctx.beginPath();

                // temp on y-axis left
                var x = 0;
                var high = 100.0;
                var step = high / yDivisions;
                for (var i = 0; i < yDivisions; i += 2) {
                    var val = parseFloat(high - i * step).toFixed(1);
                    ctx.text(val, x, canvas.yGridOffset + i * yGridStep - 2);
                }

                // flow on y-axis right
                var x = activeWidth + tickMargin + 3;
                high = 30.0;
                var step = high / yDivisions;
                for (var i = 0; i < yDivisions; i += 2) {
                    var val = parseFloat(high - i * step).toFixed(1);
                    ctx.text(val, x, canvas.yGridOffset + i * yGridStep - 2);
                }

                ctx.stroke();
                ctx.restore();
            }

            property real secondsPerPixel: (chart.endSecond - chart.startSecond) / activeWidth

            function xForIndex(start, idx, secPerPixel) {
                return tickMargin + (idx-start) / secPerPixel;
            }

            function dashedLine (ctx, x1, y1, x2, y2) {
                var dashLen = 6;

                ctx.moveTo(x1, y1);

                var dX = x2 - x1;
                var dY = y2 - y1;
                var dashes = Math.floor(Math.sqrt(dX * dX + dY * dY) / dashLen);
                var dashX = dX / dashes;
                var dashY = dY / dashes;

                for (var q = 0; q < dashes; ++q) {
                    x1 += dashX;
                    y1 += dashY;
                    if (q % 2 != 0)
                        ctx.moveTo(x1, y1);
                    else
                        ctx.lineTo(x1, y1);
                }
                if (q % 2 != 0)
                    ctx.lineTo(x2, y2);
            }

            function drawStateLabel(ctx, start, end, idx, text, colr) {
                if (start > idx || idx > end)
                    return;

                ctx.strokeStyle = colr;
                ctx.lineWidth = 1;
                ctx.font = "14px Arial";

                ctx.beginPath();

                // Cache property locally
                var secPerPixel = secondsPerPixel;

                var textW = ctx.measureText(text).width;
                var labelW = textW + 6;
                var textH = 14;
                var labelH = textH + 6;

                var lineX = xForIndex(start, idx, secPerPixel);
                var textX = (lineX + labelW <= activeWidth) ? lineX : lineX - textW;

                dashedLine(ctx, lineX, 0, lineX, height);

                // Draw label and rect
                var labelY = height - labelH - 10;
                var x1 = textX;
                var x2 = x1 + labelW;
                var y1 = labelY;
                var y2 = y1 + labelH;

                ctx.fillStyle = "white"
                ctx.fillRect(x1, y1, x2-x1+1, y2-y1+1);

                dashedLine(ctx, x1, y1, x1, y2);
                dashedLine(ctx, x1, y1, x2, y1);
                dashedLine(ctx, x1, y2, x2, y2);
                dashedLine(ctx, x2, y1, x2, y2);
                ctx.strokeText(text, x1 + 2, y2 - 4);

                ctx.stroke();
            }

            function drawStateField(ctx, start, end, field, maxValue, colr)
            {
                ctx.strokeStyle = colr;
                ctx.lineWidth = 2;
                ctx.beginPath();

                // Cache property locally
                var secPerPixel = secondsPerPixel;
                //console.log(secPerPixel, start, end);

                if (secPerPixel < 1.0) {    // one second spans multiple pixels
                    for (var i = start; i <= end; i++) {
                        var x = xForIndex(start, i, secPerPixel);
                        var y = height * (1 - stateModel.get(i)[field] / maxValue);

                        if (i == start)
                            ctx.moveTo(x, y);
                        else
                            ctx.lineTo(x, y);
                    }
                }
                else {  // one pixel spans one or more seconds
                    var width = activeWidth;
                    var margin = tickMargin;

                    //console.log("width:", width);

                    for (var dx = 0; dx < width; dx++) {
                        var i = start + ~~(dx * secPerPixel);
                        if (i > end)
                            break;

                        // Compute average of interval
                        var sum = 0;
                        for (var j=0; j<secPerPixel && i+j<=end; ++j) {
                            sum += stateModel.get(i+j)[field];
                        }

                        var x = dx + margin;
                        var y = height * (1 - sum / (j * maxValue));

                        if (i == start)
                            ctx.moveTo(x, y);
                        else
                            ctx.lineTo(x, y);
                    }
                }

                ctx.stroke();
            }

            onPaint: {
                //console.time("onPaint");
                var ctx = getContext("2d");
                ctx.globalCompositeOperation = "source-over";
                ctx.lineWidth = 1;

                drawBackground(ctx);

                ctx.save();
                var start = chart.stateModel.indexOf(chart.startSecond);
                if (start >= 0) {
                    var end = chart.stateModel.indexOf(chart.endSecond);
                    if (end < 0)
                        end = chart.stateModel.lastIndex;

                    drawStateField(ctx, start, end, "temp1", 100.0, chart.temp1Color);
                    drawStateField(ctx, start, end, "temp2", 100.0, chart.temp2Color);
                    drawStateField(ctx, start, end, "pump1_flow", 30.0, chart.pump1FlowColor);
                    drawStateField(ctx, start, end, "pump2_flow", 30.0, chart.pump2FlowColor);
                    drawStateField(ctx, start, end, "pump1_on", 1.4, chart.pump1OnColor);
                    drawStateField(ctx, start, end, "pump2_and_cooler_valve_on", 1.5, chart.pump2AndValveOnColor);
                    drawStateField(ctx, start, end, "heater_on", 1.1, chart.heaterOnColor);

                    var startStep = chart.stateModel.get(start).program_step;
                    if (startStep != 'I')
                        drawStateLabel(ctx, start, end, start, chart.stateModel.stepName(startStep), chart.stageStartColor);

                    var steps = ["M0", "M1", "M2", "M3", "B0", "B1", "B2", "B3", "C"];
                    for (var i in steps) {
                        var step = steps[i];
                        //console.log("Step:", step, "index:", chart.stateModel.stepIndex(step));
                        drawStateLabel(ctx, start, end, chart.stateModel.stepIndex(step), chart.stateModel.stepName(step), chart.stageStartColor);
                    }
                }
                ctx.restore();

                drawScales(ctx);
                //console.timeEnd("onPaint");
            }
        }
    }

    GridLayout {
        id: colorLabels

        columns: isPortrait ? parent.width / longestLabel.width : -1
        flow: isPortrait ? GridLayout.LeftToRight : GridLayout.TopToBottom
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.bottomMargin: isPortrait ? 0 : toTime.height + 8
        anchors.rightMargin: isPortrait ? 0 : canvas.tickMargin + 8
        width: isPortrait ? parent.width : longestLabel.width
        //height: isPortrait ? childrenRect.height : parent.height

        Row {
            spacing: 10
            Rectangle {
                width: 10
                height: 10
                color: chart.stageStartColor
                anchors.verticalCenter: parent.verticalCenter
            }
            Text {
                text: "Start of stage"
            }
        }

        Row {
            spacing: 10
            Rectangle {
                width: 10
                height: 10
                color: chart.temp1Color
                anchors.verticalCenter: parent.verticalCenter
            }
            Text {
                text: "Temp1"
            }
        }

        Row {
            spacing: 10
            Rectangle {
                width: 10
                height: 10
                color: chart.temp2Color
                anchors.verticalCenter: parent.verticalCenter
            }
            Text {
                text: "Temp2"
            }
        }

        Row {
            spacing: 10
            Rectangle {
                width: 10
                height: 10
                color: chart.pump1OnColor
                anchors.verticalCenter: parent.verticalCenter
            }
            Text {
                text: "Mash pump on"
            }
        }

        Row {
            spacing: 10
            Rectangle {
                width: 10
                height: 10
                color: chart.pump1FlowColor
                anchors.verticalCenter: parent.verticalCenter
            }
            Text {
                text: "Mash pump flow"
            }
        }

        Row {
            id: longestLabel
            spacing: 10
            Rectangle {
                width: 10
                height: 10
                color: chart.pump2AndValveOnColor
                anchors.verticalCenter: parent.verticalCenter
            }
            Text {
                text: "Cooling system on"
            }
        }

        Row {
            spacing: 10
            Rectangle {
                width: 10
                height: 10
                color: chart.pump2FlowColor
                anchors.verticalCenter: parent.verticalCenter
            }
            Text {
                text: "Cooler pump flow"
            }
        }

        Row {
            spacing: 10
            Rectangle {
                width: 10
                height: 10
                color: chart.heaterOnColor
                anchors.verticalCenter: parent.verticalCenter
            }
            Text {
                text: "Heater on"
            }
        }
    }
}

