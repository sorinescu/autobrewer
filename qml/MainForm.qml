import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

Item {
    id: mainForm

    property var brewState
    property alias currentView: slides.currentIndex

    ListView {
        id: slides

        width: parent.width
        anchors.fill: parent
        snapMode: ListView.SnapOneItem
        highlightRangeMode: ListView.StrictlyEnforceRange
        highlightMoveDuration: 250
        focus: false
        orientation: ListView.Horizontal
        boundsBehavior: Flickable.StopAtBounds
        currentIndex: 0

        model: VisualItemModel {
            Brewery {
                id: brewery
                width: slides.width
                height: slides.height
            }

            StateChart {
                id: chart
                width: slides.width
                height: slides.height
                stateModel: mainForm.brewState
            }
        }
    }
}
