import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import Qt.labs.settings 1.0

ListView {
    id: main
    orientation: ListView.Vertical
    spacing: 10

    property alias program: program

    BrewProgram {
        id: program
    }

    Settings {
        category: "lastProgram"
        property double mash0_temp: program.mashingSteps[0].temp
        property int mash0_time: program.mashingSteps[0].time
        property double mash1_temp: program.mashingSteps[1].temp
        property int mash1_time: program.mashingSteps[1].time
        property double mash2_temp: program.mashingSteps[2].temp
        property int mash2_time: program.mashingSteps[2].time
        property double mash3_temp: program.mashingSteps[3].temp
        property int mash3_time: program.mashingSteps[3].time
        property alias boil_time: program.boilTime
        //property alias hop_times: program.hopDeployTimes
        property int hop0_time: program.hopDeployTimes[0].time
        property int hop1_time: program.hopDeployTimes[1].time
        property double cool_temp: program.coolTemp

        Component.onCompleted: {
            // QML doesn't like alias properties to list items, e.g. this will make the component fail to load:
            // property alias foo: bar.item[0].value
            program.mashingSteps[0].temp = mash0_temp;
            program.mashingSteps[0].time = mash0_time;
            program.mashingSteps[1].temp = mash1_temp;
            program.mashingSteps[1].time = mash1_time;
            program.mashingSteps[2].temp = mash2_temp;
            program.mashingSteps[2].time = mash2_time;
            program.mashingSteps[3].temp = mash3_temp;
            program.mashingSteps[3].time = mash3_time;
            program.hopDeployTimes[0].time = hop0_time;
            program.hopDeployTimes[1].time = hop1_time;

            preventUpdates();
            for (var i=0; i<program.mashingSteps.length; ++i) {
                var step = {
                    temp: program.mashingSteps[i].temp,
                    time: program.mashingSteps[i].time,
                    minTemp: 20.0,
                    maxTemp: 75.0
                }
                //console.log("Step #"+i, "temp:", step.temp, "time:", step.time);
                if (!step.time || !step.temp)
                    break;

                mashingStepsModel.append(step);
            }
            mashingStepsModel.updateMinMaxTemps();

            for (i=0; i<program.hopDeployTimes.length; ++i) {
                var hop = {
                    time: program.hopDeployTimes[i].time,
                    minTime: 0,
                    maxTime: 200
                }
                //console.log("Hop #"+i, "time:", hop.time);
                if (hop.time < 0)
                    break;

                hopDeployModel.append(hop);
            }
            hopDeployModel.updateMinMaxDeployTimes();
        }
    }

    // When we load the values from Settings we don't want to trigger onXyzChanged events,
    // so we use a dirty trick: we mark the values as "can't update", then after 100ms we
    // mark them bask as "can update" .
    property bool _canUpdateValues: false;
    Timer {
        id: _canUpdateValuesTimer
        interval: 100

        onTriggered: _canUpdateValues = true
    }

    function preventUpdates() {
        _canUpdateValues = false;
        _canUpdateValuesTimer.running = true;
    }

    function updateProgram() {
        for (var i=0; i<program.mashingSteps.length; ++i) {
            if (i < mashingStepsModel.count) {
                var step = mashingStepsModel.get(i);
                program.mashingSteps[i].temp = step.temp;
                program.mashingSteps[i].time = step.time;
            } else {
                program.mashingSteps[i].temp = 0;
                program.mashingSteps[i].time = 0;
            }
        }
        for (i=0; i<program.hopDeployTimes.length; ++i) {
            if (i < hopDeployModel.count)
                program.hopDeployTimes[i].time = hopDeployModel.get(i).time;
            else
                program.hopDeployTimes[i].time = -1;
        }
    }

    model: VisualItemModel {
        // Mashing steps
        RowLayout {
            width: main.width
            spacing: 10

            BigText { text: "Mashing steps" }

            Item {
                Item { Layout.fillWidth: true }
            }

            Button {
                anchors.right: parent.right
                text: "Add"
                visible: mashingStepsModel.count < 4
                onClicked: {
                    var step = {
                        temp: 20.0,
                        time: 30,
                        minTemp: 20.0,
                        maxTemp: 75.0
                    }

                    if (mashingStepsModel.count) {
                        var last = mashingStepsModel.get(mashingStepsModel.count - 1);
                        step.temp = last.temp + 5.0;
                    }

                    preventUpdates();
                    mashingStepsModel.append(step);
                    //console.log("append");
                    mashingStepsModel.updateMinMaxTemps();
                    updateProgram();
                }
            }
        }

        Row {
            width: main.width

            ListModel {
                id: mashingStepsModel

                function updateMinMaxTemps() {
                    var minTemp = 19.0;
                    var step;
                    var i;
                    /*
                    console.log("updateMinMaxTemps before");
                    for (i=0; i<count; ++i) {
                        step = get(i);
                        console.log("  Item #" + i, "temp:", step.temp, "minTemp:", step.minTemp, "maxTemp:", step.maxTemp);
                    }*/
                    for (i=0; i<count; ++i) {
                        step = get(i);
                        step.minTemp = minTemp + 1.0;
                        if (step.temp < step.minTemp)
                            step.temp = step.minTemp;
                        minTemp = step.temp;
                    }
                    var maxTemp = 76.0;
                    for (i=count-1; i>=0; --i) {
                        step = get(i);
                        step.maxTemp = maxTemp - 1.0;
                        if (step.temp > step.maxTemp)
                            step.temp = step.maxTemp;
                        maxTemp = step.temp;
                    }
                    /*
                    console.log("updateMinMaxTemps after");
                    for (i=0; i<count; ++i) {
                        step = get(i);
                        console.log("  Item #" + i, "temp:", step.temp, "minTemp:", step.minTemp, "maxTemp:", step.maxTemp);
                    }*/
                }
            }

            ListView {
                model: mashingStepsModel
                orientation: Qt.Vertical
                height: currentItem ? (currentItem.height + 10) * mashingStepsModel.count  - 10: 0
                width: main.width
                clip: true
                spacing: 10
                focus: false

                delegate: RowLayout {
                    width: main.width
                    spacing: 10

                    RowLayout {
                        spacing: 10

                        ColumnLayout {
                            MediumText { text: "Temperature" }
                            MediumText { text: "Time" }
                        }

                        ColumnLayout {
                            RowLayout {
                                Slider {
                                    id: tempSlider
                                    minimumValue: minTemp // from model
                                    maximumValue: maxTemp // from model
                                    stepSize: 0.5
                                    updateValueWhileDragging: true
                                    value: temp // from model
                                    Layout.fillWidth: true

                                    onValueChanged: {
                                        // QML changes the value to previous.temp + 1 otherwise
                                        if (!_canUpdateValues)
                                            return;

                                        //console.log("setTemp:", value);
                                        mashingStepsModel.setProperty(index, "temp", value);
                                        mashingStepsModel.updateMinMaxTemps();
                                        updateProgram();
                                    }
                                }
                                MediumText { text: tempSlider.value.toFixed(1) + "°C" }
                            }

                            RowLayout {
                                Slider {
                                    id: mashTimeSlider
                                    minimumValue: 1
                                    maximumValue: 120
                                    stepSize: 1
                                    updateValueWhileDragging: true
                                    value: time // from model
                                    Layout.fillWidth: true

                                    onValueChanged: {
                                        if (!_canUpdateValues)
                                            return;
                                        mashingStepsModel.setProperty(index, "time", value);
                                        updateProgram();
                                    }
                                }
                                MediumText { text: mashTimeSlider.value + " min" }
                            }
                        }
                    }

                    Button {
                        anchors.right: parent.right
                        text: "Remove"
                        visible: mashingStepsModel.count > 1
                        onClicked: {
                            mashingStepsModel.remove(index);
                            mashingStepsModel.updateMinMaxTemps();
                            updateProgram();
                        }
                    }
                }
            }
        }

        // Boiling time
        RowLayout {
            width: main.width

            BigText { text: "Boil time" }
            Slider {
                id: boilTimeSlider
                minimumValue: 30
                maximumValue: 120
                stepSize: 1
                updateValueWhileDragging: true
                value: program.boilTime
                Layout.fillWidth: true

                onValueChanged: {
                    if (!_canUpdateValues)
                        return;
                    program.boilTime = value;
                }
            }
            MediumText { text: boilTimeSlider.value + " min" }

        }

        // Hop deploy times
        RowLayout {
            width: main.width
            spacing: 10

            BigText { text: "Hop additions" }

            Item {
                Item { Layout.fillWidth: true }
            }

            Button {
                anchors.right: parent.right
                text: "Add"
                visible: hopDeployModel.count < 2
                onClicked: {
                    var step = {
                        time: 0,
                        minTime: 0,
                        maxTime: 200    // large random value
                    }

                    if (hopDeployModel.count) {
                        var last = hopDeployModel.get(hopDeployModel.count - 1);
                        step.time = last.time + 1;
                    }

                    preventUpdates();
                    hopDeployModel.append(step);
                    //console.log("append");
                    hopDeployModel.updateMinMaxDeployTimes();
                    updateProgram();
                }
            }
        }

        Row {
            width: main.width

            ListModel {
                id: hopDeployModel

                property alias maxTime: boilTimeSlider.value

                function updateMinMaxDeployTimes() {
                    var minTime = -1;
                    var step;
                    var i;
                    /*
                    console.log("updateMinMaxDeployTimes before");
                    for (i=0; i<count; ++i) {
                        step = get(i);
                        console.log("  Item #" + i, "time:", step.time, "minTime:", step.minTime, "maxTime:", step.maxTime);
                    }*/
                    for (i=0; i<count; ++i) {
                        step = get(i);
                        step.minTime = minTime + 1;
                        if (step.time < step.minTime)
                            step.time = step.minTime;
                        minTime = step.time;
                    }
                    var maxTime_ = maxTime + 1;
                    for (i=count-1; i>=0; --i) {
                        step = get(i);
                        step.maxTime = maxTime_ - 1;
                        if (step.time > step.maxTime)
                            step.time = step.maxTime;
                        maxTime_ = step.time;
                    }
                    /*
                    console.log("updateMinMaxDeployTimes after");
                    for (i=0; i<count; ++i) {
                        step = get(i);
                        console.log("  Item #" + i, "time:", step.time, "minTime:", step.minTime, "maxTime:", step.maxTime);
                    }*/
                }

                onMaxTimeChanged: updateMinMaxDeployTimes()
            }

            ListView {
                model: hopDeployModel
                orientation: Qt.Vertical
                height: currentItem ? (currentItem.height + 10) * hopDeployModel.count  - 10: 0
                width: main.width
                clip: true
                spacing: 10
                focus: false

                delegate: RowLayout {
                    width: main.width
                    spacing: 10

                    RowLayout {
                        spacing: 10

                        MediumText { text: "Time" }

                        RowLayout {
                            Slider {
                                id: hopTimeSlider
                                minimumValue: minTime   // from model
                                maximumValue: maxTime   // from model
                                stepSize: 1
                                updateValueWhileDragging: true
                                value: time // from model
                                Layout.fillWidth: true

                                onValueChanged: {
                                    if (!_canUpdateValues)
                                        return;
                                    hopDeployModel.setProperty(index, "time", value);
                                    hopDeployModel.updateMinMaxDeployTimes();
                                    updateProgram();
                                }
                            }
                            MediumText { text: hopTimeSlider.value + " min" }
                        }
                    }

                    Button {
                        anchors.right: parent.right
                        text: "Remove"
                        visible: hopDeployModel.count > 1
                        onClicked: {
                            // QML bug: these two references can't be found after the call to remove(index)
                            var model = hopDeployModel;
                            var upd = updateProgram;
                            model.remove(index);
                            model.updateMinMaxDeployTimes();
                            upd();
                        }
                    }
                }
            }
        }

        // Cooling final temp
        RowLayout {
            width: main.width

            BigText { text: "Cooling temperature" }
            Slider {
                id: coolingTempSlider
                minimumValue: 10.0
                maximumValue: 30.0
                stepSize: 0.5
                updateValueWhileDragging: true
                value: program.coolTemp
                Layout.fillWidth: true

                onValueChanged: {
                    if (!_canUpdateValues)
                        return;
                    program.coolTemp = value;
                }
            }
            MediumText { text: coolingTempSlider.value.toFixed(1) + "°C" }

        }

    }
}
