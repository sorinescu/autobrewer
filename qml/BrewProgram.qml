import QtQuick 2.0

QtObject {
    property list<MashingStep> mashingSteps: [
        MashingStep { temp: 60.0; time: 60 },
        MashingStep {},
        MashingStep {},
        MashingStep {}
    ]
    property int boilTime: 60
    property list<HopDeployTime> hopDeployTimes: [
        HopDeployTime { time: 0 },
        HopDeployTime {}
    ]
    property double coolTemp: 20.0
}
