import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQuick.Dialogs 1.1
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import Qt.labs.settings 1.0
import ro.sorinescu.autobrewer 1.0
import "singletons"

Window {
    id: mainWindow
    title: "AutoBrewer"
    width: 1024
    height: 768
    visible: true

    Connections {
        target: BreweryController
        onErrorSignal: reportError(message)
        onCurrentStateChanged: brewState.add(CurrentBrewState)
    }

    // Platform-specific BLuetooth and/or serial config screens
    property var btConfig
    property var serialConfig

    property var conn
    onConnChanged: BreweryController.conn = conn

    BrewStateModel {
        id: brewState
    }

    Settings {
        id: settings
        category: "main_window"
        property alias x: mainWindow.x
        property alias y: mainWindow.y
        property alias width: mainWindow.width
        property alias height: mainWindow.height
    }

    PlatformDetails {
        id: platform
    }

    MessageDialog {
        id: confirmSendProgramDlg
        title: "Send program"
        text: "Are you sure you want to upload the brew program ?"
        icon: StandardIcon.Question
        standardButtons: StandardButton.Yes | StandardButton.No
        onYes: BreweryController.sendProgram(programConfigForm.program)
        onNo: console.log("Program upload rejected")
    }

    ToolBar {
        id: toolBar
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        z: 1
        style: ToolBarStyle {
            background: Rectangle {
                color: "#30000000"
            }
        }

        RowLayout {
            anchors.fill: parent
            ToolButton {
                iconSource: "qrc:/images/icon-left-arrow.png"
                visible: stack.depth > 1 || mainForm.currentView == 1
                onClicked: {
                    if (stack.currentItem === mainForm)
                        mainForm.currentView = 0;
                    else
                        stack.pop({ immediate:true });
                }
            }
            Item { Layout.fillWidth: true }
            ToolButton {
                iconSource: "qrc:/images/edit-program.png"
                onClicked: {
                    stack.push({ item:programConfigForm, immediate:true });
                }
            }
            ToolButton {
                iconSource: (conn && conn.connected && !CurrentBrewState.code) ?
                                (CurrentBrewState.running ? "qrc:/images/pause.png" : "qrc:/images/play.png") :
                                "qrc:/images/play-disabled.png"
                onClicked: {
                    if (!conn || !conn.connected || CurrentBrewState.code)
                        return;

                    // If we're idle, there's no program running, so let's upload it
                    if (CurrentBrewState.program_step == 'I')
                        confirmSendProgramDlg.open();
                    else
                        BreweryController.playOrPause(!CurrentBrewState.running);
                }
            }
            ToolButton {
                iconSource: (conn && conn.connected) ? "qrc:/images/manual.png" : "qrc:/images/manual-disabled.png"
                onClicked: {
                    if (conn && conn.connected)
                        stack.push({ item:manualControlForm, immediate:true });
                }
            }
            ToolButton {
                iconSource: (conn && conn.connected) ? "qrc:/images/connected.png" : "qrc:/images/disconnected.png"
                onClicked: {
                    if (!conn)
                        setupConnection();
                    console.log("Connect clicked:", conn && conn.connected);
                    if (conn && !conn.connected)
                        conn.reconnect()
                }
            }
            ToolButton {
                iconSource: "qrc:/images/settings.png"
                onClicked: {
                    stack.push({ item:configForm, immediate:true });
                }
            }
        }
    }

    StackView {
        id: stack
        anchors.fill: parent
        anchors.topMargin: stack.depth === 1 ? 0 : toolBar.height + 10
        anchors.leftMargin: stack.depth === 1 ? 0 : 10
        anchors.rightMargin: stack.depth === 1 ? 0 : 10
        initialItem: mainForm

        focus: true // back key navigation
        Keys.onReleased: {
            if (event.key === Qt.Key_Back && stack.depth > 1) {
                stack.pop();
                event.accepted = true;
            }
        }

        ManualControlForm {
            id: manualControlForm
            visible: false
        }

        BrewProgramConfig {
            id: programConfigForm
            visible: false
        }

        ConfigForm {
            id: configForm
            stack: stack
            visible: false
            onUseBluetoothChanged: setupConnection()
        }

        MainForm {
            id: mainForm
            brewState: brewState
        }
    }

    BrewProgramConfig {
        id: brewProgramConfig
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: toolBar.bottom
        visible: false
    }

    Rectangle {
        id: errorPopup

        anchors.centerIn: parent
        border.color: "#404040"
        color: "#e0e0e0"
        visible: text != ""
        width: errorPopupText.width + 20
        height: errorPopupText.height + 20

        property alias text: errorPopupText.text

        Text {
            id: errorPopupText
            anchors.centerIn: parent
            color: "red"
            font.pointSize: 20
            text: ""
        }

        onVisibleChanged: {
            if (visible)
                errorDisplayTimer.start();
        }

        Timer {
            id: errorDisplayTimer
            interval: 2000;
            running: false;
            onTriggered: {
                errorPopup.text = "";
            }
        }
    }

    function reportError(message) {
        errorPopup.text = message;
    }

    function setupConnection() {
        console.log("Has BT:", platform.hasBluetooth, "Has serial:", platform.hasSerialPort, "Use BT:", configForm.useBluetooth);

        if (platform.hasBluetooth && configForm.useBluetooth) {
            var connComp = Qt.createComponent("qrc:/qml/BtConnection.qml");
            conn = connComp.createObject(mainWindow);

            if (btConfig && btConfig.deviceName && btConfig.deviceAddress && btConfig.serviceUuid) {
                console.log("Using saved BT device:", btConfig.deviceName, "address:", btConfig.deviceAddress,
                            "UUID:", btConfig.serviceUuid);
                conn.connect(btConfig.deviceAddress, btConfig.serviceUuid);
            }
        }
        else if (platform.hasSerialPort && !configForm.useBluetooth) {
            conn = Qt.createQmlObject("import QtQuick 2.0; import ro.sorinescu.autobrewer 1.0; SerialPort {}", mainWindow);

            if (serialConfig.portName) {
                console.log("Using saved serial device:", serialConfig.portName);
                conn.connect(serialConfig.portName);
            }
        }
    }

    Component.onCompleted: {
        // Create Bluetooth config screen and connection only if the platform supports it
        if (platform.hasBluetooth) {
            // Configuration window
            btConfig = Qt.createQmlObject("import QtQuick 2.0; BtConfigForm { anchors.left: parent.left; anchors.right: parent.right; anchors.bottom: parent.bottom; anchors.top: toolBar.bottom; visible: false }",
                                          mainWindow);
            btConfig.errorSignal.connect(reportError);
            btConfig.currentServiceChanged.connect(function() {
                if (configForm.useBluetooth)
                    conn.connect(btConfig.currentService.deviceAddress, btConfig.currentService.serviceUuid);
            });
        }

        // Create serial config screen and connection only if the platform supports it
        if (platform.hasSerialPort) {
            // Configuration window
            serialConfig = Qt.createQmlObject("import QtQuick 2.0; SerialConfigForm { anchors.left: parent.left; anchors.right: parent.right; anchors.bottom: parent.bottom; anchors.top: toolBar.bottom; visible: false }",
                                          mainWindow);
            serialConfig.portNameChanged.connect(function() {
                if (!configForm.useBluetooth)
                    conn.connect(serialConfig.portName);
            });
        }

        setupConnection();
    }
}
