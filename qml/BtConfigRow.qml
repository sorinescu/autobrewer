import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

ColumnLayout {
    width: parent.width

    property var stack: parent.stack

    property var btConfig
    property var choiceGroup
    property bool selected

    property string deviceName: btConfig ? btConfig.deviceName : ""
    property string deviceAddress: btConfig ? btConfig.deviceAddress : ""
    property string serviceUuid: btConfig ? btConfig.serviceUuid : ""

    Row {
        id: label

        property var radio;
        property var text;

        Component.onCompleted: {
            if (choiceGroup)
                radio = Qt.createQmlObject("import QtQuick 2.0; BigRadioButton { property bool isBluetooth: true; text: 'Use Bluetooth device'; exclusiveGroup: choiceGroup; checked: selected }", label);
            else
                text = Qt.createQmlObject("import QtQuick 2.0; BigText { text: 'Bluetooth device' }", label);
        }
    }

    RowLayout {
        width: parent.width

        MediumText {
            text: deviceName ? "<b>" + deviceName + "</b><br>Address: " + deviceAddress : "<not set>"
        }

        Item {
            Layout.fillWidth: true
        }

        Button {
            text: "Select"
            onClicked: {
                btConfig.runDiscovery();
                stack.push({ item:btConfig, immediate:true });
            }
        }
    }
}
