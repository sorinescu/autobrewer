import QtQuick 2.0
import QtBluetooth 5.2
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import Qt.labs.settings 1.0
import ro.sorinescu.autobrewer 1.0

Item {
    anchors.fill: parent

    property var currentService
    property BluetoothService btService

    Settings {
        id: settings
        category: "bt_config"
        property string deviceName
        property string deviceAddress
        property string serviceUuid
    }

    property alias deviceName: settings.deviceName
    property alias deviceAddress: settings.deviceAddress
    property alias serviceUuid: settings.serviceUuid

    signal errorSignal(string message)

    onErrorSignal: console.log("BT Error:", message)

    function runDiscovery() {
        if (btDevice.hostMode === BluetoothDevice.HostPoweredOff)
            btDevice.hostMode = BluetoothDevice.HostConnectable;
        btModel.running = true;
    }

    function serviceDetails(s) {
        var str = s.deviceName + "<br>Address: " + s.deviceAddress;
        if (s.serviceName) { str += "<br>Service: " + s.serviceName; }
        if (s.serviceDescription) { str += "<br>Description: " + s.serviceDescription; }
        return str;
    }

    onCurrentServiceChanged: {
        if (deviceName !== currentService.deviceName ||
            deviceAddress !== currentService.deviceAddress ||
            serviceUuid !== currentService.serviceUuid)
        {
            console.log("New service selected:", currentService.deviceName, currentService.deviceAddress, currentService.serviceUuid);
            deviceName = currentService.deviceName;
            deviceAddress = currentService.deviceAddress;
            serviceUuid = currentService.serviceUuid;
        }
    }

    Component.onCompleted: {
        if (settings.deviceName && settings.deviceAddress && settings.serviceUuid) {
            console.log("Using saved BT device:", settings.deviceName, "address:", settings.deviceAddress,
                        "UUID:", settings.serviceUuid);
            currentService = {
                deviceName: settings.deviceName,
                deviceAddress: settings.deviceAddress,
                serviceUuid: settings.serviceUuid
            }
        }
    }

    BluetoothDevice {
        id: btDevice
    }

    BluetoothDiscoveryModel {
        id: btModel
        running: false
        discoveryMode: BluetoothDiscoveryModel.MinimalServiceDiscovery
        onDiscoveryModeChanged: console.log("Discovery mode: " + discoveryMode)
        onServiceDiscovered: console.log("Found new service " + service.deviceAddress + " " + service.deviceName + " " + service.serviceName);
        onDeviceDiscovered: console.log("New device: " + device)
        onErrorChanged: {
            switch (btModel.error) {
            case BluetoothDiscoveryModel.PoweredOffError:
                errorSignal("Bluetooth device not turned on"); break;
            case BluetoothDiscoveryModel.InputOutputError:
                errorSignal("Bluetooth I/O Error"); break;
            case BluetoothDiscoveryModel.InvalidBluetoothAdapterError:
                errorSignal("Invalid Bluetooth Adapter"); break;
            case BluetoothDiscoveryModel.NoError:
                break;
            default:
                errorSignal("Unknown Error"); break;
            }
        }
    }

    ColumnLayout {
        id: currentDevice

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        visible: currentService !== undefined

        Text {
            font.bold: true
            font.pointSize: 20
            text: "Current device"
        }
        Text {
            font.pointSize: 14
            text: currentService ? serviceDetails(currentService) : ""
        }
    }

    RowLayout {
        id: availableDevicesHdr
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.top: currentDevice.visible ? currentDevice.bottom : currentDevice.top;
        height: Math.max(loadingImg.height, scanBtn.height)

        Text {
            text: "Available devices"
            font.pointSize: 20
            font.bold: true
        }

        Item { Layout.fillWidth: true }

        Button {
            id: scanBtn
            text: "Scan"
            visible: !btModel.running
            onClicked: {
                btModel.running = true;
            }
        }
        AnimatedImage {
            id: loadingImg
            source: "qrc:/images/loading.gif"
            visible: btModel.running
        }
    }

    ListView {
        id: availableDevices
        anchors.top: availableDevicesHdr.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        clip: true

        model: btModel
        delegate: Rectangle {
            id: btDelegate
            width: parent.width
            height: column.height + 10

            property bool expanded: true;
            clip: true

            Image {
                id: bticon
                source: "qrc:/images/bluetooth.png";
                width: bttext.height;
                height: bttext.height;
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.margins: 5
            }

            Column {
                id: column
                anchors.left: bticon.right
                anchors.leftMargin: 5
                Text {
                    id: bttext
                    font.pointSize: 14
                    text: deviceName ? deviceName : name
                }

                RowLayout
                {
                    id: details
                    visible: opacity !== 0
                    opacity: btDelegate.expanded ? 1 : 0.0

                    Text {
                        font.pointSize: 12
                        text: serviceDetails(service)
                    }

                    Item { Layout.fillWidth: true }

                    Button {
                        text: "Select"
                        onClicked: {
                            currentService = service
                        }
                    }
                }
            }
        }
        focus: true
    }
}
