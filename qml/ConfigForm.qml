import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import Qt.labs.settings 1.0
import ro.sorinescu.autobrewer 1.0
import "singletons"

Column {
    id: configForm
    spacing: 10

    property var stack: parent.stack

    PlatformDetails {
        id: platform
    }

    Settings {
        id: settings
        property string brew_log_path
        property bool use_bluetooth
    }

    property alias useBluetooth: settings.use_bluetooth
    property alias brewLogPath : settings.brew_log_path

    // Brew log path
    BigText {
        text: "Brew log path"
    }

    RowLayout {
        width: parent.width

        MediumText { text: (brewLogPath && brewLogPath.replace("file://", "")) || "<not set>" }

        Item { Layout.fillWidth: true }

        Button {
            text: "Select"
            onClicked: brewLogPathDialog.open()
        }

        FileDialog {
            id: brewLogPathDialog
            title: "Brew log path"
            selectFolder: true
            folder: brewLogPath

            onFileUrlChanged: brewLogPath = fileUrl
        }
    }

    // Temp sensors
    property var _tempSensors: null

    Timer {
        running: configForm.visible
        interval: 500
        repeat: true
        onTriggered: BreweryController.askGlobalConfig()
    }
    Connections {
        target: BreweryController
        onGlobalConfigReceived: _tempSensors = config.temp_sensors;
    }

    RowLayout {
        width: parent.width

        BigText { text: "Temperature sensors" }
        Item { Layout.fillWidth: true }
        Button {
            text: "Swap"
            visible: _tempSensors && (_tempSensors[0].temp || _tempSensors[1].temp)
            onClicked: {
                // Swap sensors
                var config = {
                    temp_sensor_ids: [ _tempSensors[1].id, _tempSensors[0].id ]
                }
                BreweryController.sendGlobalConfig(config);
            }
        }
    }

    RowLayout {
        width: parent.width
        MediumText { text: "Lower vessel: " + ((_tempSensors && _tempSensors[0].temp) ? _tempSensors[0].id : "N/A") }
        Item { Layout.fillWidth: true }
        MediumText { text: (_tempSensors && _tempSensors[0].temp) ? CurrentBrewState.tempToString(_tempSensors[0].temp) : "" }
    }

    RowLayout {
        width: parent.width
        MediumText { text: "Upper vessel: " + ((_tempSensors && _tempSensors[1].temp) ? _tempSensors[1].id : "N/A") }
        Item { Layout.fillWidth: true }
        MediumText { text: (_tempSensors && _tempSensors[1].temp) ? CurrentBrewState.tempToString(_tempSensors[1].temp) : "" }
    }

    // Communications
    property var commChoiceGroup

    property var btConfigRow
    property var serialConfigRow

    Component.onCompleted: {
        if (platform.hasBluetooth && platform.hasSerialPort) {
            commChoiceGroup = Qt.createQmlObject("import QtQuick 2.0; import QtQuick.Controls 1.3; ExclusiveGroup { }", configForm);
            commChoiceGroup.currentChanged.connect(function() {
                settings.use_bluetooth = commChoiceGroup.current.isBluetooth;
            });
        }
        else
            settings.use_bluetooth = platform.hasBluetooth; // the only option

        if (platform.hasBluetooth) {
            var btConfigCompo = Qt.createComponent("qrc:/qml/BtConfigRow.qml");
            btConfigRow = btConfigCompo.createObject(configForm, { btConfig: btConfig, choiceGroup: commChoiceGroup })

            if (settings.use_bluetooth)
                btConfigRow.selected = true;
        }

        if (platform.hasSerialPort) {
            var serialConfigCompo = Qt.createComponent("qrc:/qml/SerialConfigRow.qml");
            serialConfigRow = serialConfigCompo.createObject(configForm, { serialConfig: serialConfig, choiceGroup: commChoiceGroup })

            if (!settings.use_bluetooth)
                serialConfigRow.selected = true;
        }
    }
}

