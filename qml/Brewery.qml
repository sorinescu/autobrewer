import QtQuick 2.4
import QtGraphicalEffects 1.0
import ro.sorinescu.autobrewer 1.0

Rectangle {
    property bool down_flow
    property bool up_flow
    property bool heater_on
    property bool cooler_pump_and_valve_on

    color: "white"

    Connections {
        target: CurrentBrewState
        onHeater_onChanged: { heater_on = CurrentBrewState.heater_on; }
        onPump1_onChanged: { down_flow = !CurrentBrewState.pump1_on; up_flow = CurrentBrewState.pump1_on; }
        onPump1_flowChanged: { if (!CurrentBrewState.pump1_flow) up_flow = down_flow = false; }
        onPump2_and_cooler_valve_onChanged: { cooler_pump_and_valve_on = CurrentBrewState.pump2_and_cooler_valve_on; }
    }

    BrewStateWidget {
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.topMargin: parent.height * 0.1
        anchors.rightMargin: 20
        z: 1
    }

    Image {
        id: main
        source: "image://svglayer/:/images/diagram.svg&main"
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
    }

    // Up/down flow
    Item {
        id: flows
        anchors.fill: parent

        Image {
            id: up_flow_1
            source: "image://svglayer/:/images/diagram.svg&up_flow_1"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            visible: false
        }

        Image {
            id: up_flow_2
            source: "image://svglayer/:/images/diagram.svg&up_flow_2"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            visible: false
        }

        Image {
            id: down_flow_1
            source: "image://svglayer/:/images/diagram.svg&down_flow_1"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            visible: false
        }

        Image {
            id: down_flow_2
            source: "image://svglayer/:/images/diagram.svg&down_flow_2"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            visible: false
        }

        states: [
            State { name: "flow_off"; when: (!up_flow && !down_flow) },
            State { name: "up_flow_on"; when: up_flow },
            State { name: "down_flow_on"; when: down_flow }
        ]

        transitions: [
            Transition {
                from: "up_flow_on"; to: "flow_off"
                ParallelAnimation {
                    PropertyAction { target: up_flow_1; property: "visible"; value: false }
                    PropertyAction { target: up_flow_2; property: "visible"; value: false }
                }
            },
            Transition {
                from: "flow_off"; to: "up_flow_on"
                SequentialAnimation {
                    loops: Animation.Infinite

                    ParallelAnimation {
                        PropertyAction { target: up_flow_1; property: "visible"; value: true }
                        PropertyAction { target: up_flow_2; property: "visible"; value: false }
                    }
                    PauseAnimation { duration: 200 }

                    ParallelAnimation {
                        PropertyAction { target: up_flow_1; property: "visible"; value: false }
                        PropertyAction { target: up_flow_2; property: "visible"; value: true }
                    }
                    PauseAnimation { duration: 200 }
                }
            },
            Transition {
                from: "down_flow_on"; to: "flow_off"
                ParallelAnimation {
                    PropertyAction { target: down_flow_1; property: "visible"; value: false }
                    PropertyAction { target: down_flow_2; property: "visible"; value: false }
                }
            },
            Transition {
                from: "flow_off"; to: "down_flow_on"
                SequentialAnimation {
                    loops: Animation.Infinite

                    ParallelAnimation {
                        PropertyAction { target: down_flow_1; property: "visible"; value: true }
                        PropertyAction { target: down_flow_2; property: "visible"; value: false }
                    }
                    PauseAnimation { duration: 200 }

                    ParallelAnimation {
                        PropertyAction { target: down_flow_1; property: "visible"; value: false }
                        PropertyAction { target: down_flow_2; property: "visible"; value: true }
                    }
                    PauseAnimation { duration: 200 }
                }
            }
        ]
    }

    // Heating element
    Item {
        id: heater
        anchors.fill: parent

        Image {
            id: heater_off_img
            source: "image://svglayer/:/images/diagram.svg&heater_off"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            visible: true
        }

        Image {
            id: heater_on_img
            source: "image://svglayer/:/images/diagram.svg&heater_on"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            visible: false
        }

        states: [
            State { name: "heater_off"; when: !heater_on },
            State { name: "heater_on"; when: heater_on }
        ]

        transitions: [
            Transition {
                from: "heater_off"; to: "heater_on"
                ParallelAnimation {
                    PropertyAction { target: heater_off_img; property: "visible"; value: false }
                    PropertyAction { target: heater_on_img; property: "visible"; value: true }
                }
            },
            Transition {
                from: "heater_on"; to: "heater_off"
                ParallelAnimation {
                    PropertyAction { target: heater_off_img; property: "visible"; value: true }
                    PropertyAction { target: heater_on_img; property: "visible"; value: false }
                }
            }
        ]
    }

    // Cooling coil
    Item {
        id: coolerCoil
        anchors.fill: parent

        Image {
            id: cooler_valve_off_img
            source: "image://svglayer/:/images/diagram.svg&cooler_off"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            visible: true
        }

        Image {
            id: cooler_valve_on_img
            source: "image://svglayer/:/images/diagram.svg&cooler_on"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            visible: false
        }

        states: [
            State { name: "cooler_off"; when: !cooler_pump_and_valve_on },
            State { name: "cooler_on"; when: cooler_pump_and_valve_on }
        ]

        transitions: [
            Transition {
                from: "cooler_off"; to: "cooler_on"
                ParallelAnimation {
                    PropertyAction { target: cooler_valve_off_img; property: "visible"; value: false }
                    PropertyAction { target: cooler_valve_on_img; property: "visible"; value: true }
                }
            },
            Transition {
                from: "cooler_on"; to: "cooler_off"
                ParallelAnimation {
                    PropertyAction { target: cooler_valve_off_img; property: "visible"; value: true }
                    PropertyAction { target: cooler_valve_on_img; property: "visible"; value: false }
                }
            }
        ]
    }

    // Cooling coil and pump
    Item {
        id: coolerPump
        anchors.fill: parent

        Image {
            id: cooler_flow_1
            source: "image://svglayer/:/images/diagram.svg&cooler_pump_on_1"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            visible: false
        }

        Image {
            id: cooler_flow_2
            source: "image://svglayer/:/images/diagram.svg&cooler_pump_on_2"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            visible: false
        }

        states: [
            State { name: "cooler_off"; when: !cooler_pump_and_valve_on },
            State { name: "cooler_on"; when: cooler_pump_and_valve_on }
        ]

        transitions: [
            Transition {
                from: "cooler_off"; to: "cooler_on"
                ParallelAnimation {
                    SequentialAnimation {
                        loops: Animation.Infinite

                        ParallelAnimation {
                            PropertyAction { target: cooler_flow_1; property: "visible"; value: true }
                            PropertyAction { target: cooler_flow_2; property: "visible"; value: false }
                        }
                        PauseAnimation { duration: 200 }

                        ParallelAnimation {
                            PropertyAction { target: cooler_flow_1; property: "visible"; value: false }
                            PropertyAction { target: cooler_flow_2; property: "visible"; value: true }
                        }
                        PauseAnimation { duration: 200 }
                    }
                }
            },
            Transition {
                from: "cooler_on"; to: "cooler_off"
                ParallelAnimation {
                    PropertyAction { target: cooler_flow_1; property: "visible"; value: false }
                    PropertyAction { target: cooler_flow_2; property: "visible"; value: false }
                }
            }
        ]
    }

    // Mash pump flow
    MediumText {
        x: main.x + (main.width - main.paintedWidth) / 2 + main.paintedWidth * 0.25
        y: main.y + (main.height - main.paintedHeight) / 2  + main.paintedHeight * 0.83
        horizontalAlignment: Text.AlignRight
        visible: CurrentBrewState.pump1_flow != 0
        text: CurrentBrewState.flowToString(CurrentBrewState.pump1_flow)
    }

    // Cooling pump flow
    MediumText {
        x: main.x + (main.width - main.paintedWidth) / 2 + main.paintedWidth * 0.47
        y: main.y + (main.height - main.paintedHeight) / 2  + main.paintedHeight * 0.96
        horizontalAlignment: Text.AlignRight
        visible: CurrentBrewState.pump2_flow != 0
        text: CurrentBrewState.flowToString(CurrentBrewState.pump2_flow)
    }

    // Temp1
    MediumText {
        x: main.x + (main.width - main.paintedWidth) / 2 + main.paintedWidth * 0.145
        y: main.y + (main.height - main.paintedHeight) / 2  + main.paintedHeight * 0.6
        horizontalAlignment: Text.AlignRight
        visible: CurrentBrewState.temp1 != 0
        text: CurrentBrewState.tempToString(CurrentBrewState.temp1)
    }

    // Temp2
    MediumText {
        x: main.x + (main.width - main.paintedWidth) / 2 + main.paintedWidth * 0.36
        y: main.y + (main.height - main.paintedHeight) / 2  + main.paintedHeight * 0.22
        horizontalAlignment: Text.AlignRight
        visible: CurrentBrewState.temp2 != 0
        text: CurrentBrewState.tempToString(CurrentBrewState.temp2)
    }
}

