pragma Singleton
import QtQuick 2.4
import ro.sorinescu.autobrewer 1.0

Item {
    // Bluetooth or serial connection
    property var conn: null

    signal errorSignal(string message)
    signal currentStateChanged()
    signal globalConfigReceived(var config)

    Connections {
        target: conn
        ignoreUnknownSignals: true
        onDataReceived: _handleResponse(data)
        onErrorSignal: errorSignal(message)
    }

    Timer {
        interval: 1000;
        running: true;
        repeat: true;
        onTriggered: askCurrentBrewState()
    }

    function _handleResponse(response) {
        response = response.replace(/^\s+|\s+$/g, '');
        if (!response)  // empty string
            return;

        //console.log("Got line:", response);

        // Split response into command, error code and details
        var resp_m = response.match(/^([A-Z]) (\d+)( (.+))?$/);
        if (!resp_m) {
            errorSignal("Invalid response from controller: " + response);
            return;
        }

        var cmd = resp_m[1];
        var error = parseInt(resp_m[2]);
        var details = resp_m[4];
        //console.log("Got response:", "cmd:", cmd, "error:", error, "details:", details);

        if (cmd !== 'S' && error) {
            errorSignal(errorString(error));
            return;
        }

        switch (cmd) {
        case 'S': _handleStatus(error, details); break;
        case 'Q': _handleGlobalConfig(details); break;
        case 'C': break;
        case 'M': break;
        case 'P': break;
        case 'Y': break;
        case 'R': break;
        default: errorSignal("Invalid response code from controller: " + cmd); return;
        }
    }

    function _handleStatus(error, details) {
        // <running> <time> <program_step> <temp1> <temp2> <pump1_on> <pump1_flow> <pump2_and_cooler_valve_on> <pump2_flow> <heater_on> <hop1_deploy_on> <hop2_deploy_on>
        var state_m = details.match(/^(\d+) (\d+) ([A-Z]\d*) (\d+) (\d+) (\d+) (\d+) (\d+) (\d+) (\d+) (\d+) (\d+) (\d+)$/);
        if (state_m) {
            CurrentBrewState.code = error;
            CurrentBrewState.running = parseInt(state_m[1]) != 0;
            CurrentBrewState.time = parseInt(state_m[2]);
            CurrentBrewState.program_step = state_m[3];
            CurrentBrewState.temp1 = parseInt(state_m[4]) / 100.0;
            CurrentBrewState.temp2 = parseInt(state_m[5]) / 100.0;
            CurrentBrewState.pump1_on = parseInt(state_m[6]);
            CurrentBrewState.pump1_flow = parseInt(state_m[7]) / 10.0;
            CurrentBrewState.pump2_and_cooler_valve_on = parseInt(state_m[8]);
            CurrentBrewState.pump2_flow = parseInt(state_m[9]) / 10.0;
            CurrentBrewState.heater_on = parseInt(state_m[10]);
            CurrentBrewState.hop1_deploy_on = parseInt(state_m[11])
            CurrentBrewState.hop2_deploy_on = parseInt(state_m[12])
            CurrentBrewState.heater_triac_fan_speed = parseInt(state_m[13])

            currentStateChanged();
        }
    }

    function _handleGlobalConfig(details) {
        // <temp_sensor1> [<temp_sensor2> ...]
        // - <temp_sensor>: <sensor_id> <temp>
        console.log("Got global config:", details);
        var cfg_m = details.match(/^([A-Z0-9]{16}) (\d+) ([A-Z0-9]{16}) (\d+)$/);
        if (cfg_m) {
            var settings = {
                temp_sensors: [
                    { id: cfg_m[1], temp: parseInt(cfg_m[2]) / 100.0 },
                    { id: cfg_m[3], temp: parseInt(cfg_m[4]) / 100.0 }
                ]
            }
            globalConfigReceived(settings);
        }
    }

    function askCurrentBrewState() {
        if (conn)
            conn.send("S");
    }

    function askGlobalConfig() {
        if (conn)
            conn.send("Q");
    }

    function sendCurrentBrewState() {
        if (!conn)
            return;

        // M <pump1_on> <pump2_and_cooler_valve_on> <heater_on> <hop1_deploy_on> <hop2_deploy_on>
        var cmd = "M %1 %2 %3 %4 %5"
                  .arg(CurrentBrewState.pump1_on)
                  .arg(CurrentBrewState.pump2_and_cooler_valve_on)
                  .arg(CurrentBrewState.heater_on)
                  .arg(CurrentBrewState.hop1_deploy_on)
                  .arg(CurrentBrewState.hop2_deploy_on);

        //console.log("Sending manual control:", cmd);
        conn.send(cmd);
        askCurrentBrewState();    // update state faster than 1 second
    }

    function sendGlobalConfig(config) {
        if (!conn)
            return;

        // C <sensor1_id> <sensor2_id>
        var cmd = "C %1 %2"
                  .arg(config.temp_sensor_ids[0])
                  .arg(config.temp_sensor_ids[1]);
        conn.send(cmd);
        askGlobalConfig();
    }

    function sendProgram(program) {
        if (!conn)
            return;

        // P m <mash steps> b <boil_time> <hop_deploy1_time> <hop_deploy2_time> <cool_temp>
        // - <mash step>: <mash_time> <mash_temp>
        var cmd = "P m"
        for (var i in program.mashingSteps) {
            var step = program.mashingSteps[i];
            cmd += " %1 %2".arg(step.time).arg(~~(step.temp * 100.0));
        }
        cmd += " b %1 h".arg(program.boilTime);
        for (i in program.hopDeployTimes) {
            var time = program.hopDeployTimes[i].time;
            if (time >= 0)
                cmd += " %1".arg(time);
        }
        cmd += " c %1".arg(~~(program.coolTemp * 100.0));

        console.log("Send program:", cmd);

        conn.send(cmd);
        playOrPause(true);  // also start the program
    }

    function playOrPause(play) {
        if (!conn)
            return;

        conn.send("Y %1".arg(play ? 1 : 0));
        askCurrentBrewState();    // update state faster than 1 second
    }

    function sendReset() {
        if (!conn)
            return;

        conn.send("R");
        askCurrentBrewState();    // update state faster than 1 second
    }

    // The error codes are defined in AutoBrewer.ino (enum Status)
    function errorString(error) {
        switch (error) {
        case 0: return "No error";
        case 1: return "Too much data";
        case 2: return "Invalid command";
        case 3: return "Invalid command format";
        case 4: return "Command not allowed while program is running";
        case 5: return "DC adapter current limit exceeded";
        case 6: return "Unknown or invalid state";
        case 7: return "Heater triac fan failure";
        case 8: return "Mash pump failure";
        case 9: return "Stuck sparge";
        case 10: return "Failure in cooling circuit";
        case 11: return "Whirlpool pump failure";
        case 12: return "Mash tun overflow";
        case 13: return "No program uploaded";
        default: return "Unknown error";
        }
    }
}
