import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3

RadioButton {
    style: RadioButtonStyle {
        label: BigText {
            text: control.text
        }
    }
}

