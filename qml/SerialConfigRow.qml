import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

ColumnLayout {
    width: parent.width

    property var stack: parent.stack

    property var serialConfig
    property var choiceGroup
    property bool selected

    property string portName: serialConfig ? serialConfig.portName : ""
    property string portPath: serialConfig ? serialConfig.portPath : ""

    Row {
        id: label

        property var radio;
        property var text;

        Component.onCompleted: {
            if (choiceGroup)
                radio = Qt.createQmlObject("import QtQuick 2.0; BigRadioButton { property bool isBluetooth: false; text: 'Use serial device'; exclusiveGroup: choiceGroup; checked: selected }", label);
            else
                text = Qt.createQmlObject("import QtQuick 2.0; BigText { text: 'Serial device' }", label);
        }
    }

    RowLayout {
        width: parent.width

        MediumText {
            text: portName ? "<b>" + portName + "</b><br>Path: " + portPath : "<not set>"
        }

        Item {
            Layout.fillWidth: true
        }

        Button {
            text: "Select"
            onClicked: {
                stack.push({ item:serialConfig, immediate:true });
            }
        }
    }
}
