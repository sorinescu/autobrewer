import QtQuick 2.0
import QtQuick.Layouts 1.1
import ro.sorinescu.autobrewer 1.0
import "singletons" 1.0

Rectangle {
    width: content.implicitWidth + 16
    height: content.implicitHeight + 16
    color: "#d0d0d0"
    border.color: "#404040"
    border.width: 2
    radius: 8

    Connections {
        target: BreweryController
        onCurrentStateChanged: _updateValues()
    }

    Component.onCompleted: _updateValues()

    function _updateValues() {
        var code = CurrentBrewState.code;
        stateName.text = code ? BreweryController.errorString(code) : CurrentBrewState.majorStepName;
        stateIcon.source = code ? "qrc:/images/error.png" :
                           CurrentBrewState.program_step == "I" ? "qrc:/images/idle.png" :
                           CurrentBrewState.running ? "qrc:/images/play.png" : "qrc:/images/pause.png";
    }

    GridLayout {
        id: content
        columns: 2
        anchors.fill: parent
        anchors.margins: 8

        Row {
            spacing: 8
            Layout.columnSpan: 2

            Image {
                id: stateIcon
                anchors.verticalCenter: parent.verticalCenter
                fillMode: Image.PreserveAspectFit
                height: 20
            }

            MediumText {
                id: stateName
            }
        }
    }
}

