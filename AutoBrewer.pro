TEMPLATE = app

QT += qml quick widgets svg

win32 {
    QT += serialport
    SOURCES += src/serialport.cpp
    HEADERS += src/serialport.h
}

unix:!android {
    QT += bluetooth serialport
    SOURCES += src/serialport.cpp
    HEADERS += src/serialport.h
}

android {
    QT += bluetooth
}

SOURCES += src/main.cpp \
    src/svglayerpixmapprovider.cpp \
    src/mybluetoothdevice.cpp \
    src/platformdetails.cpp \
    src/statemodel.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    src/svglayerpixmapprovider.h \
    src/mybluetoothdevice.h \
    src/platformdetails.h \
    src/statemodel.h

DISTFILES += \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/AndroidManifest.xml \
    android/gradlew.bat \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
